using Gtk 4.0;
using Adw 1;

template SettingsViewsPage : Adw.PreferencesPage {
  icon-name: "view-settings-symbolic";
  title: "Views";
  visible: true;

  Adw.PreferencesGroup {
    title: "Feed List";
    visible: true;

    Adw.ActionRow feed_list_filter_row {
      title: _("Only show relevant items");
      subtitle: _("Hide feeds and categories without unread/unstarred items");
      name: "feed_list_filter_row";
      activatable-widget: filter_feeds_switch;
      visible: true;

      Switch filter_feeds_switch {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;
      }
    }
  }

  Adw.PreferencesGroup {
    title: "Article List";
    visible: true;

    Adw.ComboRow article_order_row {
      title: _("Order");
    }

    Adw.ActionRow article_list_thumbs_row {
      title: _("Show Thumbnails");
      subtitle: _("Only if available");
      name: "article_list_thumbs_row";
      activatable-widget: show_thumbs_switch;
      visible: true;

      Switch show_thumbs_switch {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;
      }
    }

    Adw.ActionRow article_list_hide_future_row {
      title: _("Hide Future Articles");
      subtitle: _("Hide Articles dated in the Future");
      name: "article_list_hide_future_row";
      activatable-widget: hide_future_switch;
      visible: true;

      Switch hide_future_switch {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;
      }
    }
  }

  Adw.PreferencesGroup {
    title: "Article View";
    visible: true;

    Adw.ActionRow article_theme_row {
      title: _("Theme");
      name: "article_theme_row";
      visible: true;

      GestureClick article_theme_click {
        button: 1;
      }

      Box article_theme_box {
        visible: true;
        receives-default: true;
        halign: center;
        valign: center;
        spacing: 5;
        margin-start: 5;
        margin-end: 5;
        margin-top: 5;
        margin-bottom: 5;

        Label article_theme_label {
          focusable: false;
          label: _("Default");
        }

        Image {
          focusable: false;
          icon-name: "pan-down-symbolic";
        }
      }
    }

    Adw.ActionRow {
      title: _("Allow selection");
      activatable-widget: allow_selection_switch;
      visible: true;

      Switch allow_selection_switch {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;
      }
    }

    Adw.ActionRow {
      title: _("Content Width");
      subtitle: _("Width of the Article in Characters");
      visible: true;

      SpinButton content_width_spin_button {
        adjustment: contentWidthAdjustment;
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;
      }
    }

    Adw.ActionRow {
      title: _("Line Height");
      subtitle: _("Line Height in Characters");
      visible: true;

      SpinButton line_height_spin_button {
        adjustment: lineHeightAdjustment;
        digits: 1;
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;
      }
    }

    Adw.ActionRow {
      title: _("Use System Font");
      activatable-widget: use_system_font_switch;
      visible: true;

      Switch use_system_font_switch {
        margin-start: 12;
        margin-end: 12;
        margin-top: 12;
        margin-bottom: 12;
      }
    }

    Adw.ActionRow font_row {
      title: _("Font");
      activatable-widget: font_button;
      visible: true;

      FontButton font_button {
        sensitive: false;
        receives-default: true;
        valign: center;
        font: "Sans 12";
        language: "en-gb";
        preview-text: "";
        use-font: true;
      }
    }
  }
}

Adjustment contentWidthAdjustment {
  lower: 30;
  upper: 80;
  step-increment: 1;
  page-increment: 10;
  value: 50;
}

Adjustment lineHeightAdjustment {
  lower: 1.0;
  upper: 3.0;
  step-increment: 0.1;
  page-increment: 0.5;
  value: 1.8;
}
