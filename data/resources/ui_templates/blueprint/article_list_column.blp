using Gtk 4.0;
using Adw 1;

template ArticleListColumn : Box {
  name: "article_list_column";
  width-request: 360;
  hexpand: true;
  orientation: vertical;

  Adw.HeaderBar headerbar {
    show-start-title-buttons: false;
    show-end-title-buttons: false;

    [title]
    Box {
      visible: false;
    }

    ToggleButton show_sidebar_button {
      visible: false;
      receives-default: true;
      icon-name: "view-sidebar-start-symbolic";
    }

    Stack update_stack {
      transition-type: crossfade;

      StackPage {
        name: "button";
        title: _("button");
        child: Button update_button {
          receives-default: true;
          icon-name: "view-refresh-symbolic";
          tooltip-text: _("Refresh Content");
        };
      }

      StackPage {
        name: "spinner";
        title: _("spinner");
        child: Spinner {
          halign: center;
          valign: center;
          spinning: true;
        };
      }
    }

    Button offline_button {
      visible: false;
      receives-default: true;
      tooltip-text: _("Go back to online mode");
      icon-name: "network-offline-symbolic";
    }

    [end]
    ToggleButton search_button {
      receives-default: true;
      tooltip-text: _("Search Articles");
      icon-name: "system-search-symbolic";
    }

    [end]
    Stack mark_all_read_stack {
      transition-type: crossfade;

      StackPage {
        name: "button";
        title: _("button");
        child: Button mark_all_read_button {
          receives-default: true;
          icon-name: "emblem-ok-symbolic";
          tooltip-text: _("Set All/Feed/Category as read");
        };
      }

      StackPage {
        name: "spinner";
        title: _("spinner");
        child: Spinner {
          halign: center;
          valign: center;
          spinning: true;
        };
      }
    }
  }

  SearchBar search_bar {
    SearchEntry search_entry {
      hexpand: true;
    }
  }

  .ArticleList article_list {
  }

  [end]
  Adw.ViewSwitcherBar {
    stack: view_switcher_stack;
    reveal: true;
  }

  Adw.ViewStack view_switcher_stack {
    visible: false;

    Adw.ViewStackPage {
      name: "all_placeholder";
      title: _("All");
      icon-name: "format-justify-fill-symbolic";
      child: Label {};
    }

    Adw.ViewStackPage {
      name: "unread_placeholder";
      title: _("Unread");
      icon-name: "unread-symbolic";
      child: Label {};
    }

    Adw.ViewStackPage {
      name: "marked_placeholder";
      title: _("Starred");
      icon-name: "marked-symbolic";
      child: Label {};
    }
  }
}
