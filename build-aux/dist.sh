#!/bin/sh

flatpak-builder --stop-at=newsflash app ./build-aux/com.gitlab.newsflash.json
flatpak-builder --run app ./build-aux/com.gitlab.newsflash.json meson --prefix=/app _build
flatpak-builder --run app ./build-aux/com.gitlab.newsflash.json meson dist -C _build --no-tests

rm ./dist/*.tar.xz
rm ./dist/*.sha256sum

cp ./_build/meson-dist/*.tar.xz ./dist
cp ./_build/meson-dist/*.sha256sum ./dist

rm -R ./.flatpak-builder
rm -R ./_build
rm -R ./app