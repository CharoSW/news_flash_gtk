use glib::{self, IsA, ParamFlags, ParamSpec, ParamSpecObject, Value};
use gtk4::{
    prelude::*, subclass::prelude::*, Box, CompositeTemplate, Picture, Stack, StackTransitionType, Widget,
    WidgetPaintable,
};
use once_cell::sync::Lazy;
use std::cell::Cell;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/self_stack.ui")]
    pub struct SelfStack {
        #[template_child]
        pub stack: TemplateChild<Stack>,
        #[template_child]
        pub widget_paintable: TemplateChild<WidgetPaintable>,
        #[template_child]
        pub fake_widget: TemplateChild<Picture>,

        pub animation_duration_ms: Cell<u32>,
    }

    impl Default for SelfStack {
        fn default() -> Self {
            Self {
                stack: TemplateChild::default(),
                widget_paintable: TemplateChild::default(),
                fake_widget: TemplateChild::default(),

                animation_duration_ms: Cell::new(150),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SelfStack {
        const NAME: &'static str = "SelfStack";
        type Type = super::SelfStack;
        type ParentType = Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SelfStack {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![ParamSpecObject::new(
                    "child",
                    "child",
                    "child",
                    gtk4::Widget::static_type(),
                    ParamFlags::READWRITE,
                )]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "child" => {
                    let child_widget: Widget = value.get().expect("The value needs to be a widget.");
                    self.obj().set_widget(&child_widget);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "child" => self.stack.child_by_name("widget").to_value(),
                _ => unimplemented!(),
            }
        }
    }

    impl WidgetImpl for SelfStack {}

    impl BoxImpl for SelfStack {}
}

glib::wrapper! {
    pub struct SelfStack(ObjectSubclass<imp::SelfStack>)
        @extends Widget, Box;
}

impl Default for SelfStack {
    fn default() -> Self {
        Self::new()
    }
}

impl SelfStack {
    pub fn new() -> Self {
        glib::Object::new(&[])
    }

    pub fn set_transition_duration(&self, duration: u32) {
        let imp = self.imp();
        imp.animation_duration_ms.set(duration);
        imp.stack.set_transition_duration(duration);
    }

    pub fn set_widget<W: IsA<Widget>>(&self, widget: &W) {
        let imp = self.imp();
        if let Some(widget) = imp.stack.child_by_name("widget") {
            imp.stack.remove(&widget);
        }
        imp.stack.add_named(widget, Some("widget"));
        imp.widget_paintable.set_widget(Some(widget));
        imp.stack.set_visible_child_name("widget");
    }

    pub fn freeze(&self) {
        let imp = self.imp();
        let paintable = imp.widget_paintable.current_image();
        imp.fake_widget.set_paintable(Some(&paintable));

        imp.stack.set_transition_duration(0);
        imp.stack.set_visible_child_full("picture", StackTransitionType::None);
    }

    pub fn update(&self, transistion: StackTransitionType) {
        let imp = self.imp();
        imp.stack.set_transition_duration(imp.animation_duration_ms.get());
        imp.stack.set_visible_child_full("widget", transistion);
    }
}
