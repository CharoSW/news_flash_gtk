use thiserror::Error;

#[derive(Error, Debug)]
pub enum UtilError {
    #[error("IO Error")]
    IO(#[from] glib::Error),
    #[error("Error (de)serializing an object")]
    Json(#[from] serde_json::Error),
    #[error("Failed to create a cairo surface from the given data")]
    CairoSurface,
    #[error("Failed to parse svg data")]
    SVG,
    #[error("Error sending HTTP request")]
    Network(#[from] reqwest::Error),
    #[error("Timeout reached")]
    Timeout,
    #[error("Unknown Error")]
    Unknown,
}
