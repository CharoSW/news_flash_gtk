pub const TAG_DEFAULT_COLOR: &str = "#FF0077";
pub const TAG_GRADIENT_SHIFT: f64 = 0.1;
pub const TAG_TEXT_SATURATION_SHIFT: f64 = -0.15;
pub const UNKNOWN_FEED: &'static str = "Unknown Feed";
pub const NO_TITLE: &'static str = "No Title";
pub const DEFAULT_ARTICLE_CONTENT_WIDTH: u32 = 50;
pub const DEFAULT_ARTICLE_LINE_HEIGHT: f32 = 1.8;
