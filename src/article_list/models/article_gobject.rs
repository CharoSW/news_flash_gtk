use super::ArticleListArticleModel;
use crate::util::DateUtil;
use chrono::{NaiveDateTime, Utc};
use glib::{
    Boxed, Enum, Object, ObjectExt, ParamFlags, ParamSpec, ParamSpecBoxed, ParamSpecEnum, ParamSpecString, StaticType,
    ToValue, Value,
};
use gtk4::subclass::prelude::*;
use news_flash::models::{ArticleID, FeedID, Marked, Read, Tag};
use once_cell::sync::Lazy;
use parking_lot::RwLock;
use std::cell::{Cell, RefCell};
use std::sync::Arc;

mod imp {
    use super::*;

    pub struct ArticleGObject {
        pub id: RwLock<ArticleID>,
        pub title: RwLock<Arc<String>>,
        pub feed_id: RwLock<FeedID>,
        pub feed_title: RwLock<Arc<String>>,
        pub summary: RwLock<Arc<String>>,

        pub date: RefCell<GDateTime>,
        pub date_string: RefCell<String>,
        pub read: Cell<GRead>,
        pub marked: Cell<GMarked>,
        pub tags: RefCell<GTags>,
    }

    impl Default for ArticleGObject {
        fn default() -> Self {
            Self {
                id: RwLock::new(ArticleID::new("")),
                title: RwLock::new(Arc::new("".into())),
                feed_id: RwLock::new(FeedID::new("")),
                feed_title: RwLock::new(Arc::new("".into())),
                summary: RwLock::new(Arc::new("".into())),

                date: RefCell::new(Utc::now().naive_utc().into()),
                date_string: RefCell::new(DateUtil::format(&Utc::now().naive_utc())),
                read: Cell::new(GRead::Unread),
                marked: Cell::new(GMarked::Unmarked),
                tags: RefCell::new(Vec::new().into()),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleGObject {
        const NAME: &'static str = "NewsFlashArticleGObject";
        type Type = super::ArticleGObject;
    }

    impl ObjectImpl for ArticleGObject {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecEnum::new("read", "read", "read", GRead::static_type(), 0, ParamFlags::READWRITE),
                    ParamSpecEnum::new(
                        "marked",
                        "marked",
                        "marked",
                        GMarked::static_type(),
                        0,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new("date-string", "date-string", "date-string", None, ParamFlags::READWRITE),
                    ParamSpecBoxed::new("date", "date", "date", GDateTime::static_type(), ParamFlags::READWRITE),
                    ParamSpecBoxed::new("tags", "tags", "tags", GTags::static_type(), ParamFlags::READWRITE),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "read" => {
                    let input = value.get().expect("The value needs to be of type `NewsFlashGRead`.");
                    self.read.replace(input);
                }
                "marked" => {
                    let input = value.get().expect("The value needs to be of type `NewsFlashGMarked`.");
                    self.marked.replace(input);
                }
                "date" => {
                    let input = value
                        .get()
                        .expect("The value needs to be of type `NewsFlashGDateTime`.");
                    self.date.replace(input);
                }
                "date-string" => {
                    let input = value.get().expect("The value needs to be of type `string`.");
                    self.date_string.replace(input);
                }
                "tags" => {
                    let input = value.get().expect("The value needs to be of type `NewsFlashGTags`.");
                    self.tags.replace(input);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "read" => self.read.get().to_value(),
                "marked" => self.marked.get().to_value(),
                "date" => self.date.borrow().to_value(),
                "date-string" => self.date_string.borrow().to_value(),
                "tags" => self.tags.borrow().to_value(),
                _ => unimplemented!(),
            }
        }
    }
}

glib::wrapper! {
    pub struct ArticleGObject(ObjectSubclass<imp::ArticleGObject>);
}

impl ArticleGObject {
    pub fn new() -> Self {
        Object::new(&[])
    }

    pub fn from_model(model: &ArticleListArticleModel) -> Self {
        let gobject = Self::new();
        let imp = gobject.imp();

        *imp.id.write() = model.id.clone();
        *imp.title.write() = model.title.clone();
        *imp.feed_id.write() = model.feed_id.clone();
        *imp.feed_title.write() = model.feed_title.clone();
        *imp.summary.write() = model.summary.clone();

        imp.date.replace(model.date.into());
        imp.date_string.replace(DateUtil::format(&model.date));
        imp.read.replace(model.read.into());
        imp.marked.replace(model.marked.into());
        imp.tags.replace(model.tags.clone().into());
        gobject
    }

    pub fn read(&self) -> GRead {
        self.imp().read.get()
    }

    pub fn set_gread(&self, g_read: GRead) {
        self.set_property("read", g_read.to_value())
    }

    pub fn set_read(&self, read: Read) {
        let g_read: GRead = read.into();
        self.set_property("read", g_read.to_value())
    }

    pub fn marked(&self) -> GMarked {
        self.imp().marked.get()
    }

    pub fn set_gmarked(&self, g_marked: GMarked) {
        self.set_property("marked", g_marked.to_value())
    }

    pub fn set_marked(&self, marked: Marked) {
        let g_marked: GMarked = marked.into();
        self.set_property("marked", g_marked.to_value())
    }

    pub fn date(&self) -> GDateTime {
        self.imp().date.borrow().clone()
    }

    pub fn date_string(&self) -> String {
        self.imp().date_string.borrow().clone()
    }

    pub fn set_date(&self, date: NaiveDateTime) {
        self.set_property("date-string", DateUtil::format(&date));

        let g_datetime: GDateTime = date.into();
        self.set_property("date", g_datetime.to_value())
    }

    pub fn tags(&self) -> GTags {
        self.imp().tags.borrow().clone()
    }

    pub fn set_tags(&self, tags: Vec<Tag>) {
        let g_tags: GTags = tags.into();
        self.set_property("tags", g_tags.to_value())
    }

    pub fn title(&self) -> Arc<String> {
        self.imp().title.read().clone()
    }

    pub fn summary(&self) -> Arc<String> {
        self.imp().summary.read().clone()
    }

    pub fn feed_title(&self) -> Arc<String> {
        self.imp().feed_title.read().clone()
    }

    pub fn feed_id(&self) -> FeedID {
        self.imp().feed_id.read().clone()
    }

    pub fn article_id(&self) -> ArticleID {
        self.imp().id.read().clone()
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Copy, Enum)]
#[repr(u32)]
#[enum_type(name = "NewsFlashGRead")]
pub enum GRead {
    Read,
    Unread,
}

impl From<Read> for GRead {
    fn from(read: Read) -> Self {
        match read {
            Read::Read => Self::Read,
            Read::Unread => Self::Unread,
        }
    }
}

impl From<GRead> for Read {
    fn from(read: GRead) -> Self {
        match read {
            GRead::Read => Read::Read,
            GRead::Unread => Read::Unread,
        }
    }
}

impl GRead {
    pub fn invert(&self) -> Self {
        match self {
            Self::Read => Self::Unread,
            Self::Unread => Self::Read,
        }
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Copy, Enum)]
#[repr(u32)]
#[enum_type(name = "NewsFlashGMarked")]
pub enum GMarked {
    Marked,
    Unmarked,
}

impl From<Marked> for GMarked {
    fn from(marked: Marked) -> Self {
        match marked {
            Marked::Marked => Self::Marked,
            Marked::Unmarked => Self::Unmarked,
        }
    }
}

impl GMarked {
    pub fn invert(&self) -> Self {
        match self {
            Self::Marked => Self::Unmarked,
            Self::Unmarked => Self::Marked,
        }
    }
}

impl From<GMarked> for Marked {
    fn from(marked: GMarked) -> Self {
        match marked {
            GMarked::Marked => Marked::Marked,
            GMarked::Unmarked => Marked::Unmarked,
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Boxed)]
#[boxed_type(name = "NewsFlashGDateTime")]
pub struct GDateTime(NaiveDateTime);

impl From<NaiveDateTime> for GDateTime {
    fn from(dt: NaiveDateTime) -> Self {
        Self(dt)
    }
}

impl From<GDateTime> for NaiveDateTime {
    fn from(dt: GDateTime) -> Self {
        dt.0
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Boxed)]
#[boxed_type(name = "NewsFlashGTags")]
pub struct GTags(Vec<Tag>);

impl From<Vec<Tag>> for GTags {
    fn from(dt: Vec<Tag>) -> Self {
        Self(dt)
    }
}

impl From<GTags> for Vec<Tag> {
    fn from(dt: GTags) -> Self {
        dt.0
    }
}

impl<'a> From<&'a GTags> for &'a [Tag] {
    fn from(dt: &'a GTags) -> Self {
        &dt.0
    }
}
