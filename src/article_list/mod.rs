mod article_row;
mod article_tags;
mod models;

use self::article_row::ArticleRow;
use crate::app::{Action, App};
use crate::article_list::models::GRead;
use crate::content_page::ArticleListMode;
use crate::content_page::ContentPageState;
use crate::i18n::{i18n, i18n_f};
use crate::self_stack::SelfStack;
use crate::sidebar::models::SidebarSelection;
use crate::sidebar::FeedListItemID;
use crate::util::{GtkUtil, Util};
use chrono::NaiveDateTime;
use diffus::edit::{collection, Edit};
use gio::ListStore;
use glib::{clone, SourceId};
use gtk4::{
    prelude::*, subclass::prelude::*, Box, CompositeTemplate, CustomSorter, ListView, SearchBar, Shortcut,
    SignalListItemFactory, SingleSelection, Stack, StackTransitionType, Widget,
};
use gtk4::{CallbackAction, ConstantExpression, ListItem, PropertyExpression, TickCallbackId};
use libadwaita::StatusPage;
pub use models::{ArticleGObject, ArticleListArticleModel, ArticleListModel, MarkUpdate, ReadUpdate};
use news_flash::models::{ArticleID, ArticleOrder, Marked, Read, Tag};
use parking_lot::RwLock;
use std::collections::HashMap;
use std::sync::Arc;
use std::time::Duration;

const LIST_BOTTOM_THREASHOLD: f64 = 200.0;
const SCROLL_TRANSITION_DURATION: i64 = 500 * 1000;

#[derive(Debug)]
pub struct ScrollAnimationProperties {
    pub start_time: Option<i64>,
    pub end_time: Option<i64>,
    pub scroll_callback_id: Option<TickCallbackId>,
    pub transition_start_value: Option<f64>,
    pub transition_diff: Option<f64>,
}

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/article_list.ui")]
    pub struct ArticleList {
        #[template_child]
        pub stack: TemplateChild<Stack>,
        #[template_child]
        pub empty_status: TemplateChild<StatusPage>,
        #[template_child]
        pub self_stack: TemplateChild<SelfStack>,
        #[template_child]
        pub listview: TemplateChild<ListView>,
        #[template_child]
        pub factory: TemplateChild<SignalListItemFactory>,
        #[template_child]
        pub selection: TemplateChild<SingleSelection>,
        #[template_child]
        pub sorter: TemplateChild<CustomSorter>,
        #[template_child]
        pub list_store: TemplateChild<ListStore>,
        #[template_child]
        pub down_shortcut: TemplateChild<Shortcut>,
        #[template_child]
        pub up_shortcut: TemplateChild<Shortcut>,

        pub list_model: RwLock<ArticleListModel>,
        pub model_index: RwLock<HashMap<ArticleID, ArticleGObject>>,

        pub delay_next_activation: RwLock<bool>,
        pub select_after_signal: RwLock<Option<SourceId>>,
        pub selected_id: RwLock<Option<ArticleID>>,

        pub scroll_cooldown: RwLock<bool>,
        pub scroll_animation_data: RwLock<ScrollAnimationProperties>,
    }

    impl Default for ArticleList {
        fn default() -> Self {
            Self {
                stack: TemplateChild::default(),
                empty_status: TemplateChild::default(),
                self_stack: TemplateChild::default(),
                listview: TemplateChild::default(),
                factory: TemplateChild::default(),
                selection: TemplateChild::default(),
                sorter: TemplateChild::default(),
                list_store: TemplateChild::default(),
                down_shortcut: TemplateChild::default(),
                up_shortcut: TemplateChild::default(),

                list_model: RwLock::new(ArticleListModel::new(
                    &App::default().settings().read().get_article_list_order(),
                )),
                model_index: RwLock::new(HashMap::new()),

                delay_next_activation: RwLock::new(false),
                select_after_signal: RwLock::new(None),
                selected_id: RwLock::new(None),

                scroll_cooldown: RwLock::new(false),
                scroll_animation_data: RwLock::new(ScrollAnimationProperties {
                    start_time: None,
                    end_time: None,
                    scroll_callback_id: None,
                    transition_start_value: None,
                    transition_diff: None,
                }),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleList {
        const NAME: &'static str = "ArticleList";
        type ParentType = Box;
        type Type = super::ArticleList;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ArticleList {}

    impl WidgetImpl for ArticleList {}

    impl BoxImpl for ArticleList {}
}

glib::wrapper! {
    pub struct ArticleList(ObjectSubclass<imp::ArticleList>)
        @extends Widget, Box;
}

impl ArticleList {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[])
    }

    pub fn init(&self, search_bar: &SearchBar) {
        let imp = self.imp();

        let next_action = CallbackAction::new(|_widget, _| {
            App::default()
                .main_window()
                .content_page()
                .article_list_column()
                .article_list()
                .select_next_article();
            true
        });
        imp.down_shortcut.set_action(Some(&next_action));

        let prev_action = CallbackAction::new(|_widget, _| {
            App::default()
                .main_window()
                .content_page()
                .article_list_column()
                .article_list()
                .select_prev_article();
            true
        });
        imp.up_shortcut.set_action(Some(&prev_action));

        imp.factory.connect_setup(
            clone!(@weak self as this, @weak search_bar => @default-panic, move |_factory, list_item| {
                let row = ArticleRow::new();
                list_item.set_child(Some(&row));

                row.connect_local("activated", false, clone!(@weak this => @default-panic, move |args| {
                    let row = args[1].get::<ArticleRow>()
                        .expect("The value needs to be of type `ArticleRow`.");

                    let imp = this.imp();
                    let selected_article_id = imp.selection
                        .selected_item()
                        .and_then(|item| item.downcast::<ArticleGObject>().ok())
                        .map(|item| item.article_id());

                    // only activate the article if it is already selected
                    // otherwise the selection-changed signal will do the job
                    if selected_article_id == Some(row.id()) {
                        Self::activate_article(row.id(), row.read());
                    }

                    None
                }));

                // Create expression describing `list_item->item->number`
                let list_item_expression = ConstantExpression::new(list_item);
                let article_gobject_expression =
                    PropertyExpression::new(ListItem::static_type(), Some(&list_item_expression), "item");

                // Update Read
                let read_expression =
                    PropertyExpression::new(ArticleGObject::static_type(), Some(&article_gobject_expression), "read");
                read_expression.bind(&row, "read", Some(&row));

                // Update Marked
                let marked_expression = PropertyExpression::new(
                    ArticleGObject::static_type(),
                    Some(&article_gobject_expression),
                    "marked",
                );
                marked_expression.bind(&row, "marked", Some(&row));

                // Update Date
                let date_expression = PropertyExpression::new(
                    ArticleGObject::static_type(),
                    Some(&article_gobject_expression),
                    "date-string",
                );
                date_expression.bind(&row, "date", Some(&row));

                // Update Tags
                let tags_expression = PropertyExpression::new(
                    ArticleGObject::static_type(),
                    Some(&article_gobject_expression),
                    "tags",
                );
                tags_expression.bind(&row, "tags", Some(&row));
            }),
        );
        imp.factory.connect_bind(move |_factory, list_item| {
            let article = list_item.item().unwrap().downcast::<ArticleGObject>().unwrap();
            let child = list_item.child().unwrap().downcast::<ArticleRow>().unwrap();
            child.bind_model(&article);
        });

        let settings = App::default().settings();
        imp.sorter.set_sort_func(move |obj1, obj2| {
            let date_1: NaiveDateTime = obj1.downcast_ref::<ArticleGObject>().unwrap().date().into();
            let date_2: NaiveDateTime = obj2.downcast_ref::<ArticleGObject>().unwrap().date().into();

            match settings.read().get_article_list_order() {
                ArticleOrder::NewestFirst => date_2.cmp(&date_1).into(),
                ArticleOrder::OldestFirst => date_1.cmp(&date_2).into(),
            }
        });
        imp.selection.connect_selection_changed(
            clone!(@weak self as this, @weak search_bar => @default-panic, move |selection_model, _pos, _n_items| {
                let imp = this.imp();
                let selected = selection_model.selected();

                if selected == gtk4::INVALID_LIST_POSITION {
                    return;
                }

                let article_gobject = selection_model.item(selected).unwrap().downcast::<ArticleGObject>().unwrap();
                let id = article_gobject.article_id();
                let read = article_gobject.read();

                if !*imp.delay_next_activation.read() {
                    Self::activate_article(id, read);
                } else {
                    if let Some(article_gobject) = this.get_selected_article_model() {
                        let article_id = article_gobject.article_id();

                        let update = ReadUpdate {
                            article_id: article_id.clone(),
                            read: Read::Read,
                        };
                        App::default().mark_article_read(update);

                        GtkUtil::remove_source(imp.select_after_signal.write().take());

                        imp.select_after_signal.write().replace(glib::timeout_add_local(
                            Duration::from_millis(300),
                            clone!(@weak this, @weak search_bar, @strong id, @strong read => @default-panic, move || {
                                let imp = this.imp();

                                if search_bar.has_focus() {
                                    return Continue(false);
                                }

                                Self::activate_article(id.clone(), read);
                                imp.select_after_signal.write().take();
                                Continue(false)
                            }),
                        ));

                        *imp.delay_next_activation.write() = false;
                    }
                }
            }),
        );

        if let Some(vadj) = imp.listview.vadjustment() {
            vadj.connect_value_changed(clone!(@weak self as this => @default-panic, move |vadj|
            {
                let imp = this.imp();

                let is_on_cooldown = *imp.scroll_cooldown.read();
                if !is_on_cooldown {
                    let max = vadj.upper() - vadj.page_size();
                    if max > 0.0 && vadj.value() >= (max - LIST_BOTTOM_THREASHOLD) {
                        *imp.scroll_cooldown.write() = true;
                        glib::source::timeout_add_local(Duration::from_millis(800), clone!(
                            @weak vadj,
                            @weak this => @default-panic, move || {
                                let imp = this.imp();

                                *imp.scroll_cooldown.write() = false;
                                let max = vadj.upper() - vadj.page_size();
                                if max > 0.0 && vadj.value() >= (max - (LIST_BOTTOM_THREASHOLD / 4.0)) {
                                    Util::send(Action::LoadMoreArticles);
                                }
                                Continue(false)
                        }));
                        Util::send(Action::LoadMoreArticles);
                    }
                }
            }));
        }
    }

    fn activate_article(article_id: ArticleID, read: GRead) {
        if read == GRead::Unread && !App::default().content_page_state().read().get_offline() {
            let update = ReadUpdate {
                article_id: article_id.clone(),
                read: Read::Read,
            };
            App::default().mark_article_read(update);
        }

        App::default().main_window().show_article(article_id);
    }

    fn is_empty(&self) -> bool {
        let imp = self.imp();
        imp.list_store.n_items() == 0
    }

    pub fn update(
        &self,
        mut new_list: ArticleListModel,
        new_state: &Arc<RwLock<ContentPageState>>,
        is_new_list: bool,
        prev_state: &ContentPageState,
    ) {
        let imp = self.imp();

        let require_new_list = is_new_list || self.is_empty();
        let transistion = self.calc_transition_type(require_new_list, new_state, prev_state);

        imp.stack.set_transition_type(transistion);

        // check if list model is empty and display a message
        if new_list.len() == 0 {
            imp.empty_status.set_title(&Self::compose_empty_message(new_state));

            if !self.is_empty() {
                imp.stack.set_visible_child_name("empty");
                imp.list_store.remove_all();
                imp.model_index.write().clear();
            }
        } else {
            // check if a new list is reqired or current list should be updated
            if require_new_list {
                // compare with empty model
                let empty_list = ArticleListModel::new(&new_list.order());
                let diff = empty_list.generate_diff(&new_list);

                if self.is_empty() {
                    self.execute_diff(diff);
                    imp.stack.set_visible_child_name("list");
                } else {
                    imp.self_stack.freeze();
                    imp.list_store.remove_all();
                    imp.model_index.write().clear();
                    self.execute_diff(diff);
                    imp.self_stack.update(transistion);
                }
            } else {
                // don't remove selected article during update
                // the `require_new_list` check above already ensures the old and new state are the same
                // so we add the selected model to the new_list so it wont be removed during the update
                if !self.is_empty() {
                    if let Some(article_gobject) = self.get_selected_article_model() {
                        let selected_row_id = article_gobject.article_id();
                        if !new_list.contains(&selected_row_id) {
                            if let Some(selected_model) = imp.list_model.read().get_article_model(&selected_row_id) {
                                new_list.add_model(vec![selected_model.clone()]);
                            }
                        } else {
                            log::debug!("New list already contains selected article");
                        }
                    }
                }

                let model_guard = imp.list_model.read();
                let diff = model_guard.generate_diff(&new_list);
                imp.self_stack.freeze();
                self.execute_diff(diff);
                imp.self_stack.update(transistion);
            };
        }

        *imp.list_model.write() = new_list;

        // #[cfg(debug_assertions)]
        // self.check_list_integrity();
    }

    // #[cfg(debug_assertions)]
    // fn check_list_integrity(&self) {
    //     if self.is_empty() {
    //         return;
    //     }

    //     let mut table : String = "| nr | list store | model | unread |\n| --- | --- | --- | --- |".into();

    //     let imp = self.imp();
    //     let mut item = imp.listview.first_child().unwrap();
    //     let mut pos = 0;

    //     let list_item = imp.list_store.item(pos).unwrap().downcast::<ArticleGObject>().unwrap();
    //     let model_id = imp.list_model.read().get(pos as usize).unwrap().id.clone();

    //     if list_item.article_id() != model_id {
    //         log::warn!("stranger danger! list_store: {} - model: {}", list_item.article_id(), model_id);
    //     }

    //     table.push_str(&format!("\n| {} | {} | {} |", pos, list_item.article_id(), model_id));

    //     pos += 1;

    //     while let Some(next) = item.next_sibling() {
    //         let list_item = imp.list_store.item(pos).unwrap().downcast::<ArticleGObject>().unwrap();
    //         let model_id = imp.list_model.read().get(pos as usize).map(|model| model.id.clone());

    //         if Some(list_item.article_id()) != model_id {
    //             log::warn!("stranger danger! list_store: {} - model: {:?}", list_item.article_id(), model_id);
    //         }

    //         table.push_str(&format!("\n| {} | {} | {:?} | {:?} |", pos, list_item.article_id(), model_id, list_item.read()));

    //         pos += 1;
    //         item = next;
    //     }

    //     #[allow(deprecated)]
    //     std::fs::write(&format!("{}/dbg.md", std::env::home_dir().unwrap().display()), table).unwrap();
    // }

    pub fn add_more_articles(&self, list_to_append: ArticleListModel) {
        let imp = self.imp();

        let g_articles = list_to_append
            .models()
            .iter()
            .map(|model| ArticleGObject::from_model(model))
            .collect::<Vec<ArticleGObject>>();

        imp.list_store.splice(imp.list_store.n_items(), 0, &g_articles);

        for g_article in g_articles {
            if !imp.list_model.read().contains(&g_article.article_id()) {
                imp.model_index.write().insert(g_article.article_id(), g_article);
            } else {
                log::warn!("trying to add article which already exists: {}", g_article.article_id());
            }
        }

        imp.list_model.write().add_model(list_to_append.models().clone());

        // #[cfg(debug_assertions)]
        // self.check_list_integrity();
    }

    fn execute_diff(&self, diff: Edit<'_, Vec<ArticleListArticleModel>>) {
        let imp = self.imp();
        let mut pos = 0;
        let mut model_index_guard = imp.model_index.write();
        let mut article_insert_batch: Vec<ArticleID> = Vec::new();

        match diff {
            Edit::Copy(_list) => { /* no difference */ }
            Edit::Change(diff) => {
                let mut iter = diff.into_iter().peekable();

                loop {
                    let edit = iter.next();

                    if let Some(edit) = edit {
                        match edit {
                            collection::Edit::Copy(_article) => {
                                // nothing changed
                                pos += 1;
                            }
                            collection::Edit::Insert(article) => {
                                let article_id = article.id.clone();
                                let article_gobject = ArticleGObject::from_model(article);

                                // if the next edit will be an insert as well add it to g_article_vec
                                // and insert all g_articles at once as soon as the next edit
                                // is something other than insert
                                if let Some(collection::Edit::Insert(_next_article)) = iter.peek() {
                                    article_insert_batch.push(article_gobject.article_id().clone());
                                    model_index_guard.insert(article_id, article_gobject);
                                } else if !article_insert_batch.is_empty() {
                                    article_insert_batch.push(article_gobject.article_id().clone());
                                    model_index_guard.insert(article_id, article_gobject);

                                    let g_article_to_insert = article_insert_batch
                                        .iter()
                                        .filter_map(|id| model_index_guard.get(id))
                                        .cloned()
                                        .collect::<Vec<ArticleGObject>>();

                                    imp.list_store.splice(pos as u32, 0, &g_article_to_insert);
                                    pos += article_insert_batch.len();

                                    article_insert_batch.clear();
                                } else {
                                    imp.list_store.insert(pos as u32, &article_gobject);
                                    model_index_guard.insert(article_id, article_gobject);
                                    pos += 1;
                                }
                            }
                            collection::Edit::Remove(article) => {
                                let remove_pos = model_index_guard
                                    .get(&article.id)
                                    .and_then(|article_gobject| imp.list_store.find(article_gobject));
                                if let Some(remove_pos) = remove_pos {
                                    imp.list_store.remove(remove_pos);
                                    model_index_guard.remove(&article.id);
                                }
                            }
                            collection::Edit::Change(diff) => {
                                if let Some(article_gobject) = model_index_guard.get(&diff.id) {
                                    if let Some(read) = diff.read {
                                        article_gobject.set_read(read);
                                    }
                                    if let Some(marked) = diff.marked {
                                        article_gobject.set_marked(marked);
                                    }
                                    if let Some(timestamp) = diff.date {
                                        article_gobject.set_date(timestamp);
                                    }
                                    if let Some(tags) = diff.tags {
                                        article_gobject.set_tags(tags);
                                    }
                                }
                                pos += 1;
                            }
                        }
                    } else {
                        break;
                    }
                }
            }
        }
    }

    fn calc_transition_type(
        &self,
        require_new_list: bool,
        new_state: &Arc<RwLock<ContentPageState>>,
        prev_state: &ContentPageState,
    ) -> StackTransitionType {
        if require_new_list {
            match prev_state.get_article_list_mode() {
                ArticleListMode::All => match new_state.read().get_article_list_mode() {
                    ArticleListMode::All => {}
                    ArticleListMode::Unread | ArticleListMode::Marked => return StackTransitionType::SlideLeft,
                },
                ArticleListMode::Unread => match new_state.read().get_article_list_mode() {
                    ArticleListMode::All => return StackTransitionType::SlideRight,
                    ArticleListMode::Unread => {}
                    ArticleListMode::Marked => return StackTransitionType::SlideLeft,
                },
                ArticleListMode::Marked => match new_state.read().get_article_list_mode() {
                    ArticleListMode::All | ArticleListMode::Unread => return StackTransitionType::SlideRight,
                    ArticleListMode::Marked => {}
                },
            }
        }
        StackTransitionType::Crossfade
    }

    fn compose_empty_message(new_state: &RwLock<ContentPageState>) -> String {
        match new_state.read().get_sidebar_selection() {
            SidebarSelection::All => match new_state.read().get_article_list_mode() {
                ArticleListMode::All => match new_state.read().get_search_term() {
                    Some(search) => i18n_f("No articles that fit \"{}\".", &[&search]),
                    None => i18n("No articles."),
                },
                ArticleListMode::Unread => match new_state.read().get_search_term() {
                    Some(search) => i18n_f("No unread articles that fit \"{}\".", &[&search]),
                    None => i18n("No unread articles."),
                },
                ArticleListMode::Marked => match new_state.read().get_search_term() {
                    Some(search) => i18n_f("No starred articles that fit \"{}\".", &[&search]),
                    None => i18n("No starred articles."),
                },
            },
            SidebarSelection::FeedList(id, title) => {
                let item = match id {
                    FeedListItemID::Category(_) => "category",
                    FeedListItemID::Feed(..) => "feed",
                };
                match new_state.read().get_article_list_mode() {
                    ArticleListMode::All => match new_state.read().get_search_term() {
                        Some(search) => i18n_f("No articles that fit \"{}\" in {} \"{}\".", &[&search, item, &title]),
                        None => i18n_f("No articles in {} \"{}\".", &[item, &title]),
                    },
                    ArticleListMode::Unread => match new_state.read().get_search_term() {
                        Some(search) => i18n_f(
                            "No unread articles that fit \"{}\" in {} \"{}\".",
                            &[&search, item, &title],
                        ),
                        None => i18n_f("No unread articles in {} \"{}\".", &[item, &title]),
                    },
                    ArticleListMode::Marked => match new_state.read().get_search_term() {
                        Some(search) => i18n_f(
                            "No starred articles that fit \"{}\" in {} \"{}\".",
                            &[&search, item, &title],
                        ),
                        None => i18n_f("No starred articles in {} \"{}\".", &[item, &title]),
                    },
                }
            }
            SidebarSelection::Tag(_id, title) => match new_state.read().get_article_list_mode() {
                ArticleListMode::All => match new_state.read().get_search_term() {
                    Some(search) => i18n_f("No articles that fit \"{}\" in tag \"{}\".", &[&search, &title]),
                    None => i18n_f("No articles in tag \"{}\".", &[&title]),
                },
                ArticleListMode::Unread => match new_state.read().get_search_term() {
                    Some(search) => i18n_f("No unread articles that fit \"{}\" in tag \"{}\".", &[&search, &title]),
                    None => i18n_f("No unread articles in tag \"{}\".", &[&title]),
                },
                ArticleListMode::Marked => match new_state.read().get_search_term() {
                    Some(search) => i18n_f("No starred articles that fit \"{}\" in tag \"{}\".", &[&search, &title]),
                    None => i18n_f("No starred articles in tag \"{}\".", &[&title]),
                },
            },
        }
    }

    pub fn select_next_article(&self) {
        self.select_article(1)
    }

    pub fn select_prev_article(&self) {
        self.select_article(-1)
    }

    fn select_article(&self, direction: i32) {
        let imp = self.imp();

        if !self.is_empty() {
            let selected_item_pos = imp.selection.selected();
            if selected_item_pos == gtk4::INVALID_LIST_POSITION {
                return self.select_first();
            }

            let new_item_pos = selected_item_pos as i32 + direction;

            if new_item_pos < 0 || new_item_pos as u32 == gtk4::INVALID_LIST_POSITION {
                return self.select_first();
            }

            // select now, but activate 300ms later
            *imp.delay_next_activation.write() = true;
            imp.selection.select_item(new_item_pos as u32, true);
            if let Some(article_gobject) = self.get_selected_article_model() {
                if let Some(allocated_height) = self.get_allocated_row_height(&article_gobject.article_id()) {
                    self.animate_scroll_diff(f64::from(direction * allocated_height));
                }
            }
        }
    }

    fn select_first(&self) {
        let imp = self.imp();

        *imp.delay_next_activation.write() = true;
        imp.selection.select_item(0, true);
        self.animate_scroll_absolute(0.0);
    }

    pub fn get_selected_article_model(&self) -> Option<ArticleGObject> {
        if !self.is_empty() {
            let imp = self.imp();
            imp.selection
                .selected_item()
                .and_then(|item| item.downcast::<ArticleGObject>().ok())
        } else {
            None
        }
    }

    pub fn set_article_row_state(&self, article_id: &ArticleID, read: Option<Read>, marked: Option<Marked>) {
        if !self.is_empty() {
            let imp = self.imp();
            if let Some(article_gobject) = imp.model_index.read().get(article_id) {
                if let Some(read) = read {
                    article_gobject.set_read(read);
                    imp.list_model.write().set_read(article_id, read);
                }
                if let Some(marked) = marked {
                    article_gobject.set_marked(marked);
                    imp.list_model.write().set_marked(article_id, marked);
                }
            }
        }
    }

    pub fn article_row_update_tags(&self, article_id: &ArticleID, add: Option<&Tag>, remove: Option<&Tag>) {
        if !self.is_empty() {
            let imp = self.imp();
            if let Some(article_gobject) = imp.model_index.read().get(article_id) {
                if let Some(tag) = remove {
                    if let Some(tags) = imp.list_model.write().remove_tag(article_id, tag) {
                        article_gobject.set_tags(tags.clone());
                    }
                }
                if let Some(tag) = add {
                    if let Some(tags) = imp.list_model.write().add_tag(article_id, tag) {
                        article_gobject.set_tags(tags.clone());
                    }
                }
            }
        }
    }

    pub fn get_last_row_model(&self) -> Option<ArticleListArticleModel> {
        self.imp().list_model.read().last().cloned()
    }

    fn get_allocated_row_height(&self, _id: &ArticleID) -> Option<i32> {
        // FIXME: how to access ListItem from listview to get allocated height?
        Some(106)
        // Some(105)
    }

    fn animate_scroll_diff(&self, diff: f64) {
        let pos = self.get_scroll_value() + diff;
        self.animate_scroll_absolute(pos)
    }

    fn animate_scroll_absolute(&self, pos: f64) {
        let imp = self.imp();

        let animate = match gtk4::Settings::default() {
            Some(settings) => settings.is_gtk_enable_animations(),
            None => false,
        };

        if !self.is_mapped() || !animate {
            return self.set_scroll_value(pos);
        }

        imp.scroll_animation_data.write().start_time = self.frame_clock().map(|clock| clock.frame_time());
        imp.scroll_animation_data.write().end_time = imp
            .listview
            .frame_clock()
            .map(|clock| clock.frame_time() + SCROLL_TRANSITION_DURATION);

        let callback_id = imp.scroll_animation_data.write().scroll_callback_id.take();

        let leftover_scroll = match callback_id {
            Some(callback_id) => {
                let start_value = Util::some_or_default(imp.scroll_animation_data.read().transition_start_value, 0.0);
                let diff_value = Util::some_or_default(imp.scroll_animation_data.read().transition_diff, 0.0);

                callback_id.remove();
                start_value + diff_value - self.get_scroll_value()
            }
            None => 0.0,
        };

        imp.scroll_animation_data
            .write()
            .transition_diff
            .replace(if (pos + 1.0).abs() < 0.001 {
                self.get_scroll_upper() - self.get_scroll_page_size() - self.get_scroll_value()
            } else {
                (pos - self.get_scroll_value()) + leftover_scroll
            });

        imp.scroll_animation_data
            .write()
            .transition_start_value
            .replace(self.get_scroll_value());

        imp.scroll_animation_data
            .write()
            .scroll_callback_id
            .replace(imp.listview.add_tick_callback(clone!(
                @weak self as this => @default-panic, move |widget, clock| {

                let imp = this.imp();

                let start_value = Util::some_or_default(imp.scroll_animation_data.read().transition_start_value, 0.0);
                let diff_value = Util::some_or_default(imp.scroll_animation_data.read().transition_diff, 0.0);
                let now = clock.frame_time();
                let end_time_value = Util::some_or_default(imp.scroll_animation_data.read().end_time, 0);
                let start_time_value = Util::some_or_default(imp.scroll_animation_data.read().start_time, 0);

                if !widget.is_mapped() {
                    this.set_scroll_value(start_value + diff_value);
                    return Continue(false);
                }

                if imp.scroll_animation_data.read().end_time.is_none() {
                    return Continue(false);
                }

                let t = if now < end_time_value {
                    (now - start_time_value) as f64 / (end_time_value - start_time_value) as f64
                } else {
                    1.0
                };

                let t = Util::ease_out_cubic(t);

                this.set_scroll_value(start_value + (t * diff_value));

                if let Some(vadjustment) = imp.listview.vadjustment() {
                    if vadjustment.value() <= 0.0 || now >= end_time_value {
                        imp.listview.queue_draw();
                        let mut scroll_animation_data_guard = imp.scroll_animation_data.write();
                        scroll_animation_data_guard.transition_start_value.take();
                        scroll_animation_data_guard.transition_diff.take();
                        scroll_animation_data_guard.start_time.take();
                        scroll_animation_data_guard.end_time.take();
                        scroll_animation_data_guard.scroll_callback_id.take();
                        return Continue(false);
                    }
                }

                Continue(true)
            })));
    }

    fn set_scroll_value(&self, pos: f64) {
        let imp = self.imp();

        if let Some(vadjustment) = imp.listview.vadjustment() {
            let pos = if (pos + 1.0).abs() < 0.001 {
                vadjustment.upper() - vadjustment.page_size()
            } else {
                pos
            };
            vadjustment.set_value(pos);
        }
    }

    fn get_scroll_value(&self) -> f64 {
        let imp = self.imp();

        if let Some(vadjustment) = imp.listview.vadjustment() {
            vadjustment.value()
        } else {
            0.0
        }
    }

    fn get_scroll_upper(&self) -> f64 {
        let imp = self.imp();

        if let Some(vadjustment) = imp.listview.vadjustment() {
            vadjustment.upper()
        } else {
            0.0
        }
    }

    fn get_scroll_page_size(&self) -> f64 {
        let imp = self.imp();

        if let Some(vadjustment) = imp.listview.vadjustment() {
            vadjustment.page_size()
        } else {
            0.0
        }
    }

    pub fn get_model(&self, article_id: &ArticleID) -> Option<ArticleGObject> {
        self.imp().model_index.read().get(article_id).cloned()
    }
}
