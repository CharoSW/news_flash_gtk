use crate::add_popover::AddPopover;
use crate::app::App;
use crate::util::{GtkUtil, Util, CHANNEL_ERROR, RUNTIME_ERROR};
use feedly_api::models::SearchResultItem;
use futures::channel::oneshot;
use futures::FutureExt;
use glib::clone;
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{CompositeTemplate, GestureClick, Image, Label};
use news_flash::models::Url;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/discover_search_item.ui")]
    pub struct SearchItemRow {
        #[template_child]
        pub search_item_row_click: TemplateChild<GestureClick>,
        #[template_child]
        pub search_item_title: TemplateChild<Label>,
        #[template_child]
        pub search_item_description: TemplateChild<Label>,
        #[template_child]
        pub search_item_image: TemplateChild<Image>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SearchItemRow {
        const NAME: &'static str = "SearchItemRow";
        type ParentType = gtk4::ListBoxRow;
        type Type = super::SearchItemRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SearchItemRow {}

    impl WidgetImpl for SearchItemRow {}

    impl ListBoxRowImpl for SearchItemRow {}
}

glib::wrapper! {
    pub struct SearchItemRow(ObjectSubclass<imp::SearchItemRow>)
        @extends gtk4::Widget, gtk4::ListBoxRow;
}

impl SearchItemRow {
    pub fn new(item: &SearchResultItem) -> Self {
        let row = glib::Object::new::<Self>(&[]);
        let imp = row.imp();

        let search_item_feed_url = Self::feedly_id_to_rss_url(&item.feed_id);
        imp.search_item_row_click.connect_released(clone!(
            @weak row => @default-panic, move |_gesture, times, _x, _y|
        {
            if times != 1 {
                return
            }

            row.grab_focus();

            if let Some(search_item_feed_url) = &search_item_feed_url {
                let add_pop = AddPopover::new_for_feed_url(&search_item_feed_url);
                add_pop.set_autohide(true);
                add_pop.set_parent(&row);
                add_pop.connect_closed(|popover| {
                    popover.unparent();
                });
                add_pop.popup();
            }
        }));

        let scale = GtkUtil::get_scale(&imp.search_item_image.get());

        imp.search_item_title.set_label(
            &item
                .title
                .clone()
                .expect("Empty titles should not be created in the first place!"),
        );

        let description = if let Some(description) = &item.description {
            description.replace('\n', " ").replace('\r', " ").replace('_', " ")
        } else {
            "No description".to_owned()
        };

        imp.search_item_description.set_label(&description);

        let icon_url = if let Some(visual_url) = &item.visual_url {
            Some(visual_url.clone())
        } else if let Some(logo) = &item.logo {
            Some(logo.clone())
        } else if let Some(icon_url) = &item.icon_url {
            Some(icon_url.clone())
        } else {
            None
        };

        if let Some(icon_url) = icon_url {
            let (sender, receiver) = oneshot::channel::<Option<Vec<u8>>>();

            let thread_future = async move {
                let runtime = tokio::runtime::Builder::new_current_thread()
                    .enable_all()
                    .build()
                    .expect(RUNTIME_ERROR);
                let client = Util::build_client();

                let res = runtime.block_on(async {
                    match client.get(&icon_url).send().await {
                        Ok(response) => match response.bytes().await {
                            Ok(bytes) => Some(Vec::from(bytes.as_ref())),
                            Err(_) => None,
                        },
                        Err(_) => None,
                    }
                });

                sender.send(res).expect(CHANNEL_ERROR);
            };

            let search_item_image = imp.search_item_image.get();
            let glib_future = receiver.map(move |res| {
                if let Some(byte_vec) = res.expect(CHANNEL_ERROR) {
                    if let Ok(surface) = GtkUtil::create_texture_from_bytes(&byte_vec, 64, 64, scale) {
                        search_item_image.set_from_paintable(Some(&surface));
                    }
                }
            });

            App::default().threadpool().spawn_ok(thread_future);
            Util::glib_spawn_future(glib_future);
        }

        row
    }

    fn feedly_id_to_rss_url(feedly_id: &str) -> Option<Url> {
        let url_string: String = feedly_id.chars().skip(5).collect();
        if let Ok(url) = Url::parse(&url_string) {
            Some(url)
        } else {
            None
        }
    }
}
