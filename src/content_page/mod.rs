mod article_list_column;
mod article_list_mode;
mod articleview_column;
mod content_page_state;
mod offline_popover;
mod online_popover;
mod sidebar_column;

pub use self::article_list_column::ArticleListColumn;
pub use self::article_list_mode::ArticleListMode;
pub use self::articleview_column::ArticleViewColumn;
pub use self::content_page_state::ContentPageState;
pub use self::offline_popover::OfflinePopover;
pub use self::online_popover::OnlinePopover;
pub use self::sidebar_column::SidebarColumn;

use crate::app::{Action, App};
use crate::article_list::ArticleListModel;
use crate::error::NewsFlashGtkError;
use crate::i18n::i18n_f;
use crate::responsive::ResponsiveLayout;
use crate::settings::Settings;
use crate::sidebar::models::SidebarSelection;
use crate::sidebar::{FeedListItemID, FeedListTree, TagListModel};
use crate::undo_action::UndoAction;
use crate::util::{Util, CHANNEL_ERROR};
use chrono::{DateTime, Duration, TimeZone, Utc};
use eyre::{Result, WrapErr};
use futures::channel::oneshot;
use futures_util::future::FutureExt;
use glib::{clone, subclass};
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{CompositeTemplate, Separator};
use libadwaita::{Flap, Leaflet, Toast, ToastOverlay};
use log::warn;
use news_flash::error::NewsFlashError;
use news_flash::models::{
    ArticleFilter, ArticleOrder, CategoryID, FeedID, Marked, PluginCapabilities, PluginID, Read, TagID,
    NEWSFLASH_TOPLEVEL,
};
use parking_lot::RwLock;
use std::collections::HashSet;
use std::sync::Arc;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/content_page.ui")]
    pub struct ContentPage {
        #[template_child]
        pub content_overlay: TemplateChild<ToastOverlay>,
        #[template_child]
        pub flap: TemplateChild<Flap>,
        #[template_child]
        pub leaflet: TemplateChild<Leaflet>,
        #[template_child]
        pub separator: TemplateChild<Separator>,
        #[template_child]
        pub article_list_column: TemplateChild<ArticleListColumn>,
        #[template_child]
        pub sidebar_column: TemplateChild<SidebarColumn>,
        #[template_child]
        pub articleview_column: TemplateChild<ArticleViewColumn>,

        pub prev_settings: RwLock<Option<Settings>>,
        pub state: Arc<RwLock<ContentPageState>>,
        pub prev_state: Arc<RwLock<ContentPageState>>,

        pub toasts: Arc<RwLock<HashSet<Toast>>>,
        pub current_undo_action: Arc<RwLock<Option<UndoAction>>>,
        pub processing_undo_actions: Arc<RwLock<HashSet<UndoAction>>>,
        pub responsive_layout: Arc<RwLock<Option<ResponsiveLayout>>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ContentPage {
        const NAME: &'static str = "ContentPage";
        type ParentType = gtk4::Box;
        type Type = super::ContentPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ContentPage {}

    impl WidgetImpl for ContentPage {}

    impl BoxImpl for ContentPage {}
}

glib::wrapper! {
    pub struct ContentPage(ObjectSubclass<imp::ContentPage>)
        @extends gtk4::Widget, gtk4::Box;
}

impl ContentPage {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[])
    }

    pub fn init(&self) {
        let imp = self.imp();

        imp.sidebar_column.init();
        imp.article_list_column.init();
        imp.articleview_column.init();

        imp.responsive_layout.write().replace(ResponsiveLayout::new(self));

        imp.prev_settings
            .write()
            .replace(App::default().settings().read().clone());
    }

    pub fn state(&self) -> Arc<RwLock<ContentPageState>> {
        self.imp().state.clone()
    }

    pub fn prev_state(&self) -> Arc<RwLock<ContentPageState>> {
        self.imp().prev_state.clone()
    }

    pub fn simple_error(&self, message: &str) {
        let imp = self.imp();
        let toast = Toast::new(message);
        toast.connect_dismissed(clone!(@weak self as this => @default-panic, move |toast| {
            let imp = this.imp();
            imp.toasts.write().remove(toast);
        }));
        imp.content_overlay.add_toast(&toast);
        imp.toasts.write().insert(toast);
    }

    pub fn newsflash_error(&self, message: &str, error: NewsFlashError) {
        let imp = self.imp();
        let toast = Toast::new(message);
        toast.set_button_label(Some("details"));
        App::default().set_newsflash_error(NewsFlashGtkError::NewsFlash {
            source: error,
            context: message.into(),
        });
        toast.set_action_name(Some("win.show-error-dialog"));
        toast.connect_dismissed(clone!(@weak self as this => @default-panic, move |toast| {
            let imp = this.imp();
            imp.toasts.write().remove(toast);
        }));
        imp.content_overlay.add_toast(&toast);
        imp.toasts.write().insert(toast);
    }

    pub fn dismiss_notifications(&self) {
        let imp = self.imp();

        for toast in imp.toasts.write().drain() {
            toast.dismiss();
        }
    }

    pub fn remove_current_undo_action(&self) {
        let imp = self.imp();
        let _ = imp.current_undo_action.write().take();

        // update lists
        Util::send(Action::UpdateSidebar);
        Util::send(Action::UpdateArticleList);
    }

    pub fn add_undo_notification(&self, action: UndoAction) {
        let imp = self.imp();

        if let Some(current_action) = imp.current_undo_action.write().take() {
            log::debug!("remove current action: {}", current_action);
            Self::execute_action(&current_action, &imp.processing_undo_actions);

            // dismiss all other toasts
            for toast in imp.toasts.write().drain() {
                toast.dismiss();
            }
        }

        let message = match &action {
            UndoAction::DeleteCategory(_id, label) => i18n_f("Deleted Category '{}'", &[label]),
            UndoAction::DeleteFeed(_id, label) => i18n_f("Deleted Feed '{}'", &[label]),
            UndoAction::DeleteTag(_id, label) => i18n_f("Deleted Tag '{}'", &[label]),
        };

        let toast = Toast::new(&message);
        toast.set_button_label(Some("undo"));
        toast.set_action_name(Some("win.remove-undo-action"));
        toast.connect_dismissed(clone!(
            @strong action,
            @weak self as this => @default-panic, move |toast| {
                let imp = this.imp();

                // if this is locked we're dismissing all toasts anyway
                if imp.current_undo_action.is_locked_exclusive() {
                    return;
                }

                if let Some(current_action) = imp.current_undo_action.write().take() {
                    log::debug!("remove current action: {}", current_action);
                    Self::execute_action(&current_action, &imp.processing_undo_actions);
                };

                imp.toasts.write().remove(toast);
        }));
        imp.content_overlay.add_toast(&toast);
        imp.toasts.write().insert(toast);

        imp.current_undo_action.write().replace(action);

        // update lists
        Util::send(Action::UpdateSidebar);
        Util::send(Action::UpdateArticleList);
    }

    pub fn execute_pending_undoable_action(&self) {
        let imp = self.imp();

        if let Some(current_action) = self.get_current_undo_action() {
            Self::execute_action(&current_action, &imp.processing_undo_actions);
        }
    }

    pub fn get_current_undo_action(&self) -> Option<UndoAction> {
        self.imp().current_undo_action.read().as_ref().map(|a| a.clone())
    }

    pub fn processing_undo_actions(&self) -> Arc<RwLock<HashSet<UndoAction>>> {
        self.imp().processing_undo_actions.clone()
    }

    fn execute_action(action: &UndoAction, processing_actions: &Arc<RwLock<HashSet<UndoAction>>>) {
        processing_actions.write().insert(action.clone());
        let callback = Box::new(
            clone!(@strong processing_actions, @strong action => @default-panic, move || {
                processing_actions.write().remove(&action);
            }),
        );
        match action {
            UndoAction::DeleteFeed(feed_id, _label) => {
                App::default().delete_feed(feed_id.clone(), callback);
            }
            UndoAction::DeleteCategory(category_id, _label) => {
                App::default().delete_category(category_id.clone(), callback);
            }
            UndoAction::DeleteTag(tag_id, _label) => {
                App::default().delete_tag(tag_id.clone(), callback);
            }
        }
    }

    pub fn sidebar_column(&self) -> &SidebarColumn {
        let imp = self.imp();
        &imp.sidebar_column
    }

    pub fn article_list_column(&self) -> &ArticleListColumn {
        let imp = self.imp();
        &imp.article_list_column
    }

    pub fn articleview_column(&self) -> &ArticleViewColumn {
        let imp = self.imp();
        &imp.articleview_column
    }

    pub fn flap(&self) -> &Flap {
        let imp = self.imp();
        &imp.flap
    }

    pub fn leaflet(&self) -> &Leaflet {
        let imp = self.imp();
        &imp.leaflet
    }

    pub fn responsive_layout(&self) -> ResponsiveLayout {
        let imp = self.imp();
        imp.responsive_layout
            .read()
            .clone()
            .expect("ContentPage not initialized")
    }

    pub fn load_branding(&self) -> bool {
        if let Some(id) = App::default().news_flash().read().as_ref().and_then(|n| n.id()) {
            let user_name = App::default().news_flash().read().as_ref().and_then(|n| n.user_name());
            if self.load_branding_impl(&id, user_name.as_deref()) {
                // try to fill content page with data
                self.update_sidebar();
                self.update_article_list();
            }
            true
        } else {
            false
        }
    }

    fn load_branding_impl(&self, id: &PluginID, user_name: Option<&str>) -> bool {
        if self.sidebar_column().set_account(id, user_name).is_err() {
            Util::send(Action::SimpleMessage("Failed to set service logo.".to_owned()));
            false
        } else {
            true
        }
    }

    pub fn clear(&self) {
        let imp = self.imp();
        let settings = App::default().settings();
        self.articleview_column().article_view().close_article();
        self.state().write().set_prefer_scraped_content(false);

        let empty_list_model = ArticleListModel::new(&settings.read().get_article_list_order());
        self.article_list_column().article_list().update(
            empty_list_model,
            &self.state(),
            true,
            &self.prev_state().read().clone(),
        );

        let feed_tree_model = FeedListTree::new();
        self.sidebar_column().sidebar().update_feedlist(feed_tree_model);

        let tag_list_model = TagListModel::new();
        self.sidebar_column().sidebar().update_taglist(tag_list_model);
        self.sidebar_column().sidebar().hide_taglist();
        imp.prev_settings.write().replace(settings.read().clone());
        *self.prev_state().write() = self.state().read().clone();
    }

    fn is_new_article_list(&self) -> bool {
        let imp = self.imp();
        let settings = App::default().settings();

        // Is this a new list based on changed persitent settings?
        let new_list_because_of_settings = if let Some(prev_settings) = imp.prev_settings.read().as_ref() {
            settings.read().get_article_list_order() != prev_settings.get_article_list_order()
                || settings.read().get_article_list_show_thumbs() != prev_settings.get_article_list_show_thumbs()
        } else {
            false
        };
        // Is this a new list based on changed window state? (e.g. different sidebar selection)
        let new_list_because_of_window_state = *self.state().read() != *self.prev_state().read();
        new_list_because_of_settings || new_list_because_of_window_state
    }

    pub fn update_article_list(&self) {
        let (sender, receiver) = oneshot::channel::<Result<ArticleListModel, NewsFlashError>>();

        let is_new_list = self.is_new_article_list();
        let article_filter = self.update_article_list_filter(is_new_list);
        let more_articles_filter = self.load_more_articles_filter();
        let mut list_model = ArticleListModel::new(&App::default().settings().read().get_article_list_order());

        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let mut articles = match news_flash.get_articles(article_filter) {
                    Ok(articles) => articles,
                    Err(error) => {
                        sender.send(Err(error)).expect(CHANNEL_ERROR);
                        return;
                    }
                };

                let loaded_article_count = articles.len() as i64;
                if loaded_article_count < ContentPageState::page_size() && !is_new_list {
                    // article list is not filled all the way up to "page size"
                    // load a few more
                    let mut more_articles = match news_flash.get_articles(more_articles_filter) {
                        Ok(articles) => articles,
                        Err(error) => {
                            sender.send(Err(error)).expect(CHANNEL_ERROR);
                            return;
                        }
                    };
                    articles.append(&mut more_articles);
                }

                let feeds = match news_flash.get_feeds() {
                    Ok((feeds, _)) => feeds,
                    Err(error) => {
                        sender.send(Err(error)).expect(CHANNEL_ERROR);
                        return;
                    }
                };
                let (tags, taggings) = match news_flash.get_tags() {
                    Ok(res) => res,
                    Err(error) => {
                        sender.send(Err(error)).expect(CHANNEL_ERROR);
                        return;
                    }
                };
                let articles = articles
                    .drain(..)
                    .map(|article| {
                        let feed = feeds.iter().find(|f| f.feed_id == article.feed_id);
                        let taggings: HashSet<&TagID> = taggings
                            .iter()
                            .filter(|t| t.article_id == article.article_id)
                            .map(|t| &t.tag_id)
                            .collect();
                        let tags = tags.iter().filter(|t| taggings.contains(&t.tag_id)).collect::<Vec<_>>();

                        (article, feed, tags)
                    })
                    .collect();

                list_model.add(articles);

                sender.send(Ok(list_model)).expect(CHANNEL_ERROR);
            }
        };

        let glib_future = receiver.map(clone!(@weak self as this => @default-panic, move |res| {
            if let Ok(res) = res {
                if let Ok(article_list_model) = res {
                    this
                        .article_list_column()
                        .article_list()
                        .update(article_list_model, &this.state(), is_new_list, &*this.prev_state().read());
                }
            }

            this.imp()
                .prev_settings
                .write()
                .replace(App::default().settings().read().clone());
            *this.prev_state().write() = this.state().read().clone();
        }));

        App::default().threadpool().spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    pub fn load_more_articles(&self) {
        let (sender, receiver) = oneshot::channel::<Result<ArticleListModel, NewsFlashError>>();
        let article_filter = self.load_more_articles_filter();
        let mut list_model = ArticleListModel::new(&App::default().settings().read().get_article_list_order());

        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let mut articles = match news_flash.get_articles(article_filter) {
                    Ok(articles) => articles,
                    Err(error) => {
                        sender.send(Err(error)).expect(CHANNEL_ERROR);
                        return;
                    }
                };

                let feeds = match news_flash.get_feeds() {
                    Ok((feeds, _)) => feeds,
                    Err(error) => {
                        sender.send(Err(error)).expect(CHANNEL_ERROR);
                        return;
                    }
                };

                let (tags, taggings) = match news_flash.get_tags() {
                    Ok(res) => res,
                    Err(error) => {
                        sender.send(Err(error)).expect(CHANNEL_ERROR);
                        return;
                    }
                };

                let articles = articles
                    .drain(..)
                    .map(|article| {
                        let feed = feeds.iter().find(|f| f.feed_id == article.feed_id);
                        let taggings: HashSet<&TagID> = taggings
                            .iter()
                            .filter(|t| t.article_id == article.article_id)
                            .map(|t| &t.tag_id)
                            .collect();
                        let tags = tags.iter().filter(|t| taggings.contains(&t.tag_id)).collect::<Vec<_>>();

                        (article, feed, tags)
                    })
                    .collect();

                list_model.add(articles);

                sender.send(Ok(list_model)).expect(CHANNEL_ERROR);
            }
        };

        let article_list = self.article_list_column().article_list().clone();
        let glib_future = receiver.map(move |res| {
            if let Ok(res) = res {
                if let Ok(article_list_model) = res {
                    article_list.add_more_articles(article_list_model);
                }
            }
        });

        App::default().threadpool().spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn should_load_unread(&self) -> Option<Read> {
        match self.state().read().get_article_list_mode() {
            ArticleListMode::All | ArticleListMode::Marked => None,
            ArticleListMode::Unread => Some(Read::Unread),
        }
    }

    fn should_load_marked(&self) -> Option<Marked> {
        match self.state().read().get_article_list_mode() {
            ArticleListMode::All | ArticleListMode::Unread => None,
            ArticleListMode::Marked => Some(Marked::Marked),
        }
    }

    fn should_load_sidebar_selection(&self) -> (Option<FeedID>, Option<CategoryID>, Option<TagID>) {
        let (selected_feed, selected_category, selected_tag) = match self.state().read().get_sidebar_selection() {
            SidebarSelection::All => (None, None, None),
            SidebarSelection::Tag(id, _) => (None, None, Some(id.clone())),
            SidebarSelection::FeedList(id, _title) => match id {
                FeedListItemID::Feed(id, _) => (Some(id.clone()), None, None),
                FeedListItemID::Category(id) => (None, Some(id.clone()), None),
            },
        };

        (selected_feed, selected_category, selected_tag)
    }

    fn should_hide_future_articles(&self, older_than: Option<DateTime<Utc>>) -> Option<DateTime<Utc>> {
        let hide_furure_articles = App::default().settings().read().get_article_list_hide_future_articles();

        if hide_furure_articles {
            if let Some(older_than) = older_than {
                if older_than < Utc::now() {
                    Some(older_than)
                } else {
                    Some(Utc::now())
                }
            } else {
                Some(Utc::now())
            }
        } else {
            older_than
        }
    }

    fn load_articles_blacklist(&self) -> (Option<Vec<FeedID>>, Option<Vec<CategoryID>>) {
        let current_undo_action = self.get_current_undo_action();
        let processing_undo_actions = self.processing_undo_actions();

        let mut undo_actions = Vec::new();
        let mut feed_blacklist = Vec::new();
        let mut category_blacklist = Vec::new();

        if let Some(current_undo_action) = current_undo_action {
            undo_actions.push(current_undo_action);
        }

        for processing_undo_action in &*processing_undo_actions.read() {
            undo_actions.push(processing_undo_action.clone());
        }

        for undo_action in undo_actions {
            match undo_action {
                UndoAction::DeleteFeed(feed_id, _label) => feed_blacklist.push(feed_id.clone()),
                UndoAction::DeleteCategory(category_id, _label) => category_blacklist.push(category_id.clone()),
                UndoAction::DeleteTag(_tag_id, _label) => {}
            }
        }

        let feed_blacklist = if feed_blacklist.is_empty() {
            None
        } else {
            Some(feed_blacklist)
        };
        let category_blacklist = if category_blacklist.is_empty() {
            None
        } else {
            Some(category_blacklist)
        };

        (feed_blacklist, category_blacklist)
    }

    fn update_article_list_filter(&self, is_new_list: bool) -> ArticleFilter {
        let last_article_in_list_date = self
            .article_list_column()
            .article_list()
            .get_last_row_model()
            .map(|model| model.date);

        let (limit, last_article_date) = if is_new_list {
            (ContentPageState::page_size(), None)
        } else {
            if last_article_in_list_date.is_none() {
                // article list is empty: load default amount of articles to fill it
                (ContentPageState::page_size(), None)
            } else {
                (i64::MAX, last_article_in_list_date)
            }
        };

        let (older_than, newer_than) = if let Some(last_article_date) = last_article_date {
            match App::default().settings().read().get_article_list_order() {
                // +/- 1s to not exclude last article in list
                ArticleOrder::NewestFirst => (
                    None,
                    Some(Utc.from_utc_datetime(&last_article_date) - Duration::seconds(1)),
                ),
                ArticleOrder::OldestFirst => (
                    Some(Utc.from_utc_datetime(&last_article_date) + Duration::seconds(1)),
                    None,
                ),
            }
        } else {
            (None, None)
        };

        let search_term = self.state().read().get_search_term().clone();
        let order = App::default().settings().read().get_article_list_order();

        // mutate older_than to hide articles in the future
        let older_than = self.should_hide_future_articles(older_than);

        let unread = self.should_load_unread();
        let marked = self.should_load_marked();
        let (selected_feed, selected_category, selected_tag) = self.should_load_sidebar_selection();
        let (feed_blacklist, category_blacklist) = self.load_articles_blacklist();

        ArticleFilter {
            limit: Some(limit),
            offset: None,
            order: Some(order),
            unread,
            marked,
            feeds: selected_feed.map(|f| vec![f]),
            feed_blacklist,
            categories: selected_category.map(|c| vec![c]),
            category_blacklist,
            tags: selected_tag.map(|t| vec![t]),
            ids: None,
            newer_than,
            older_than,
            search_term,
        }
    }

    fn load_more_articles_filter(&self) -> ArticleFilter {
        let search_term = self.state().read().get_search_term().clone();
        let order = App::default().settings().read().get_article_list_order();

        let last_article_in_list_date = self
            .article_list_column()
            .article_list()
            .get_last_row_model()
            .map(|model| Utc.from_utc_datetime(&model.date));

        let (older_than, newer_than) = if let Some(last_article_date) = last_article_in_list_date {
            match order {
                // newest first => we want articles older than the last in list
                ArticleOrder::NewestFirst => (Some(last_article_date), None),

                // newsest first => we want article newer than the last in list
                ArticleOrder::OldestFirst => (None, Some(last_article_date)),
            }
        } else {
            (None, None)
        };

        // mutate older_than to hide articles in the future
        let older_than = self.should_hide_future_articles(older_than);

        let unread = self.should_load_unread();
        let marked = self.should_load_marked();
        let (selected_feed, selected_category, selected_tag) = self.should_load_sidebar_selection();
        let (feed_blacklist, category_blacklist) = self.load_articles_blacklist();

        ArticleFilter {
            limit: Some(ContentPageState::page_size()),
            offset: None,
            order: Some(order),
            unread,
            marked,
            feeds: selected_feed.map(|f| vec![f]),
            feed_blacklist,
            categories: selected_category.map(|c| vec![c]),
            category_blacklist,
            tags: selected_tag.map(|t| vec![t]),
            ids: None,
            newer_than,
            older_than,
            search_term,
        }
    }

    pub fn update_sidebar(&self) {
        let (glib_sender, receiver) = oneshot::channel::<Result<(i64, FeedListTree, Option<TagListModel>)>>();

        let state = self.state();
        let current_undo_action = self.get_current_undo_action();
        let processing_undo_actions = self.processing_undo_actions();
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let mut tree = FeedListTree::new();
                let mut tag_list_model: Option<TagListModel> = None;

                let (categories, category_mappings) =
                    match news_flash.get_categories().wrap_err("Failed to load categories") {
                        Ok(categories) => categories,
                        Err(error) => {
                            glib_sender.send(Err(error)).expect(CHANNEL_ERROR);
                            return;
                        }
                    };
                let (feeds, mut feed_mappings) = match news_flash.get_feeds().wrap_err("Failed to load feeds") {
                    Ok(res) => res,
                    Err(error) => {
                        glib_sender.send(Err(error)).expect(CHANNEL_ERROR);
                        return;
                    }
                };

                // collect unread and marked counts
                let hide_future_articles = App::default().settings().read().get_article_list_hide_future_articles();
                let feed_count_map = match state.read().get_article_list_mode() {
                    ArticleListMode::All | ArticleListMode::Unread => {
                        match news_flash
                            .unread_count_feed_map(hide_future_articles)
                            .wrap_err("Failed to load unread counts")
                        {
                            Ok(res) => res,
                            Err(error) => {
                                glib_sender.send(Err(error)).expect(CHANNEL_ERROR);
                                return;
                            }
                        }
                    }
                    ArticleListMode::Marked => match news_flash
                        .marked_count_feed_map()
                        .wrap_err("Failed to load marked counts")
                    {
                        Ok(res) => res,
                        Err(error) => {
                            glib_sender.send(Err(error)).expect(CHANNEL_ERROR);
                            return;
                        }
                    },
                };

                let mut pending_delte_feeds = HashSet::new();
                let mut pending_delete_categories = HashSet::new();
                let mut pending_delete_tags = HashSet::new();
                if let Some(current_undo_action) = &current_undo_action {
                    match current_undo_action {
                        UndoAction::DeleteFeed(id, _label) => pending_delte_feeds.insert(id),
                        UndoAction::DeleteCategory(id, _label) => pending_delete_categories.insert(id),
                        UndoAction::DeleteTag(id, _label) => pending_delete_tags.insert(id),
                    };
                }
                let processing_undo_actions = &*processing_undo_actions.read();
                for processing_undo_action in processing_undo_actions {
                    match processing_undo_action {
                        UndoAction::DeleteFeed(id, _label) => pending_delte_feeds.insert(&id),
                        UndoAction::DeleteCategory(id, _label) => pending_delete_categories.insert(&id),
                        UndoAction::DeleteTag(id, _label) => pending_delete_tags.insert(&id),
                    };
                }

                // If there are feeds without a category:
                // - create mappings for all feeds without category to be children of the toplevel
                let mut uncategorized_mappings = Util::create_mappings_for_uncategorized_feeds(&feeds, &feed_mappings);
                feed_mappings.append(&mut uncategorized_mappings);

                // feedlist: Categories
                for mapping in &category_mappings {
                    if pending_delete_categories.contains(&mapping.category_id) {
                        continue;
                    }

                    let category = match categories.iter().find(|c| c.category_id == mapping.category_id) {
                        Some(res) => res,
                        None => {
                            warn!(
                                "Mapping for category '{}' exists, but can't find the category itself",
                                mapping.category_id
                            );
                            Util::send(Action::SimpleMessage(format!(
                                "Sidebar: missing category with id: '{}'",
                                mapping.category_id
                            )));
                            continue;
                        }
                    };

                    let category_item_count = Util::calculate_item_count_for_category(
                        &category.category_id,
                        &categories,
                        &feed_mappings,
                        &category_mappings,
                        &feed_count_map,
                        &pending_delte_feeds,
                        &pending_delete_categories,
                    );

                    if let Err(error) = tree
                        .add_category(category, &mapping, category_item_count)
                        .wrap_err("Failed to add category to tree")
                    {
                        glib_sender.send(Err(error)).expect(CHANNEL_ERROR);
                        return;
                    }
                }

                // feedlist: Feeds
                for mapping in &feed_mappings {
                    if pending_delte_feeds.contains(&mapping.feed_id)
                        || pending_delete_categories.contains(&mapping.category_id)
                    {
                        continue;
                    }

                    let feed = match feeds.iter().find(|feed| feed.feed_id == mapping.feed_id) {
                        Some(res) => res,
                        None => {
                            warn!(
                                "Mapping for feed '{}' exists, but can't find the feed itself",
                                mapping.feed_id
                            );
                            Util::send(Action::SimpleMessage(format!(
                                "Sidebar: missing feed with id: '{}'",
                                mapping.feed_id
                            )));
                            continue;
                        }
                    };

                    let item_count = match feed_count_map.get(&mapping.feed_id) {
                        Some(count) => *count,
                        None => 0,
                    };
                    if tree.add_feed(&feed, &mapping, item_count).is_err() {}
                }

                // tag list
                let support_tags = App::default()
                    .features()
                    .read()
                    .contains(PluginCapabilities::SUPPORT_TAGS);

                if support_tags {
                    let mut list = TagListModel::new();
                    let (tags, _taggings) = match news_flash.get_tags().wrap_err("Failed to load tags") {
                        Ok(res) => res,
                        Err(error) => {
                            glib_sender.send(Err(error)).expect(CHANNEL_ERROR);
                            return;
                        }
                    };

                    if !tags.is_empty() {
                        for tag in tags {
                            if pending_delete_tags.contains(&tag.tag_id) {
                                continue;
                            }
                            if let Err(error) = list.add(&tag).wrap_err("Failed to add tag to list") {
                                glib_sender.send(Err(error)).expect(CHANNEL_ERROR);
                                return;
                            }
                        }
                    }

                    tag_list_model = Some(list);
                }

                //let total_item_count = feed_count_map.iter().map(|(_key, value)| value).sum();
                let total_item_count = Util::calculate_item_count_for_category(
                    &NEWSFLASH_TOPLEVEL,
                    &categories,
                    &feed_mappings,
                    &category_mappings,
                    &feed_count_map,
                    &pending_delte_feeds,
                    &pending_delete_categories,
                );

                glib_sender
                    .send(Ok((total_item_count, tree, tag_list_model)))
                    .expect(CHANNEL_ERROR);
            }
        };

        let sidebar = self.sidebar_column().sidebar().clone();
        let glib_future = receiver.map(move |res| match res {
            Ok(res) => match res {
                Ok((total_count, feed_list_model, tag_list_model)) => {
                    sidebar.update_feedlist(feed_list_model);
                    sidebar.update_all(total_count);
                    if let Some(tag_list_model) = tag_list_model {
                        if tag_list_model.is_empty() {
                            sidebar.hide_taglist();
                        } else {
                            sidebar.update_taglist(tag_list_model);
                            sidebar.show_taglist();
                        }
                    } else {
                        sidebar.hide_taglist();
                    }
                }
                Err(error) => Util::send(Action::SimpleMessage(format!("Failed to update sidebar: '{}'", error))),
            },
            Err(error) => Util::send(Action::SimpleMessage(format!("Failed to update sidebar: '{}'", error))),
        });

        App::default().threadpool().spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    pub fn article_view_scroll_diff(&self, diff: f64) {
        self.articleview_column().article_view().animate_scroll_diff(diff)
    }

    pub fn sidebar_select_next_item(&self) {
        self.sidebar_column().sidebar().select_next_item()
    }

    pub fn sidebar_select_prev_item(&self) {
        self.sidebar_column().sidebar().select_prev_item()
    }

    pub fn set_offline(&self, offline: bool) {
        self.state().write().set_offline(offline);
        self.articleview_column().set_offline(offline);
        self.article_list_column().set_offline(offline);
        self.sidebar_column().set_offline(offline);
    }

    pub fn enter_fullscreen_article(&self) {
        self.set_sidebar_and_article_list_visiblility(false);
    }

    pub fn leave_fullscreen_article(&self) {
        self.set_sidebar_and_article_list_visiblility(true);
    }

    pub fn enter_fullscreen_video(&self) {
        self.set_sidebar_and_article_list_visiblility(false);
        self.set_article_view_chrome_visibility(false);
    }

    pub fn leave_fullscreen_video(&self) {
        self.set_sidebar_and_article_list_visiblility(true);
        self.set_article_view_chrome_visibility(true);
    }

    fn set_article_view_chrome_visibility(&self, visible: bool) {
        let imp = self.imp();

        if visible {
            imp.articleview_column.headerbar().show();
            imp.articleview_column.footer_revealer().show();
        } else {
            imp.articleview_column.headerbar().hide();
            imp.articleview_column.footer_revealer().hide();
        }
    }

    fn set_sidebar_and_article_list_visiblility(&self, visible: bool) {
        let imp = self.imp();

        if visible {
            imp.article_list_column.show();
            imp.sidebar_column.show();
            imp.separator.show();

            imp.leaflet.set_visible_child(&*imp.articleview_column);
        } else {
            imp.article_list_column.hide();
            imp.sidebar_column.hide();
            imp.separator.hide();
        }
    }
}
