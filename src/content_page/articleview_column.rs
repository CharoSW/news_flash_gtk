use crate::app::App;
use crate::article_view::ArticleView;
use crate::enclosure_popover::EnclosurePopover;
use crate::i18n::i18n;
use crate::share::share_popover::SharePopover;
use crate::tag_popover::TagPopover;
use crate::util::GtkUtil;
use gio::Menu;
use glib::{clone, SignalHandlerId};
use gtk4::{
    prelude::*, subclass::prelude::*, Button, CompositeTemplate, GestureClick, MenuButton, PositionType, Revealer,
    Stack, ToggleButton,
};
use libadwaita::{HeaderBar, Squeezer};
use news_flash::models::{Enclosure, FatArticle, Marked, PluginCapabilities, Read};
use parking_lot::RwLock;
use std::sync::Arc;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/articleview_column.ui")]
    pub struct ArticleViewColumn {
        #[template_child]
        pub article_view: TemplateChild<ArticleView>,
        #[template_child]
        pub back_button: TemplateChild<Button>,
        #[template_child]
        pub headerbar: TemplateChild<HeaderBar>,
        #[template_child]
        pub footer_revealer: TemplateChild<Revealer>,
        #[template_child]
        pub header_squeezer: TemplateChild<Squeezer>,
        #[template_child]
        pub scrap_content_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub footer_scrap_content_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub enclosure_button: TemplateChild<MenuButton>,
        #[template_child]
        pub footer_enclosure_button: TemplateChild<MenuButton>,
        #[template_child]
        pub share_button: TemplateChild<MenuButton>,
        #[template_child]
        pub footer_share_button: TemplateChild<MenuButton>,
        #[template_child]
        pub scrap_content_stack: TemplateChild<Stack>,
        #[template_child]
        pub footer_scrap_content_stack: TemplateChild<Stack>,
        #[template_child]
        pub tag_button: TemplateChild<MenuButton>,
        #[template_child]
        pub tag_button_click: TemplateChild<GestureClick>,
        #[template_child]
        pub footer_tag_button: TemplateChild<MenuButton>,
        #[template_child]
        pub footer_tag_button_click: TemplateChild<GestureClick>,
        #[template_child]
        pub more_actions_button: TemplateChild<MenuButton>,
        #[template_child]
        pub more_actions_stack: TemplateChild<Stack>,
        #[template_child]
        pub mark_article_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub mark_article_read_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub mark_article_stack: TemplateChild<Stack>,
        #[template_child]
        pub mark_article_read_stack: TemplateChild<Stack>,

        pub tag_popover: TagPopover,
        pub footer_tag_popover: TagPopover,

        pub share_popover: SharePopover,
        pub footer_share_popover: SharePopover,

        pub enclosure_popover: EnclosurePopover,
        pub footer_enclosure_popover: EnclosurePopover,

        pub scrap_content_event: Arc<RwLock<Option<SignalHandlerId>>>,
        pub footer_scrap_content_event: Arc<RwLock<Option<SignalHandlerId>>>,

        pub mark_article_event: Arc<RwLock<Option<SignalHandlerId>>>,
        pub mark_article_read_event: Arc<RwLock<Option<SignalHandlerId>>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleViewColumn {
        const NAME: &'static str = "ArticleViewColumn";
        type ParentType = gtk4::Box;
        type Type = super::ArticleViewColumn;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ArticleViewColumn {}

    impl WidgetImpl for ArticleViewColumn {}

    impl BoxImpl for ArticleViewColumn {}
}

glib::wrapper! {
    pub struct ArticleViewColumn(ObjectSubclass<imp::ArticleViewColumn>)
        @extends gtk4::Widget, gtk4::Box;
}

impl ArticleViewColumn {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[])
    }

    pub fn init(&self) {
        let imp = self.imp();
        imp.article_view.init();

        self.setup_more_actions_button();
        self.update_share_popover();
        Self::setup_scrap_content_button(
            &imp.scrap_content_event,
            &imp.footer_scrap_content_event,
            &imp.scrap_content_button,
            &imp.footer_scrap_content_button,
        );
        self.setup_mark_buttons();

        imp.tag_button.set_popover(Some(&imp.tag_popover));
        imp.footer_tag_button.set_popover(Some(&imp.footer_tag_popover));
        imp.tag_popover.set_position(PositionType::Bottom);
        imp.footer_tag_popover.set_position(PositionType::Top);

        imp.share_button.set_popover(Some(&imp.share_popover));
        imp.footer_share_button.set_popover(Some(&imp.footer_share_popover));
        imp.share_popover.set_position(PositionType::Bottom);
        imp.footer_share_popover.set_position(PositionType::Top);

        self.show_article(None, None);
    }

    fn setup_mark_buttons(&self) {
        let imp = self.imp();

        imp.mark_article_event
            .write()
            .replace(imp.mark_article_button.connect_toggled(clone!(
                @weak self as column => @default-panic, move |toggle_button|
            {
                let imp = column.imp();

                if toggle_button.is_active() {
                    imp.mark_article_stack.set_visible_child_name("marked");
                } else {
                    imp.mark_article_stack.set_visible_child_name("unmarked");
                }
                App::default().toggle_selected_article_marked();
            })));

        imp.mark_article_read_event
            .write()
            .replace(imp.mark_article_read_button.connect_toggled(clone!(
                @weak self as column => @default-panic, move |toggle_button|
            {
                let imp = column.imp();

                if toggle_button.is_active() {
                    imp.mark_article_read_stack.set_visible_child_name("unread");
                } else {
                    imp.mark_article_read_stack.set_visible_child_name("read");
                }
                App::default().toggle_selected_article_read();
            })));
    }

    fn setup_scrap_content_button(
        scrap_content_event: &Arc<RwLock<Option<SignalHandlerId>>>,
        footer_scrap_content_event: &Arc<RwLock<Option<SignalHandlerId>>>,
        scrap_content_button: &ToggleButton,
        footer_scrap_content_button: &ToggleButton,
    ) {
        scrap_content_event
            .write()
            .replace(scrap_content_button.connect_toggled(clone!(
                @weak footer_scrap_content_button => @default-panic, move |button|
            {
                footer_scrap_content_button.set_active(button.is_active());
                if button.is_active() {
                    App::default().content_page_state().write().set_prefer_scraped_content(true);
                    App::default().start_grab_article_content();
                } else {
                    App::default().content_page_state().write().set_prefer_scraped_content(false);
                    App::default().main_window().content_page().articleview_column().article_view().redraw_article();
                }
            })));

        footer_scrap_content_event
            .write()
            .replace(footer_scrap_content_button.connect_toggled(clone!(
                @weak scrap_content_button => @default-panic, move |button|
            {
                // only trigger the top button: this will in turn send the action to the app
                if scrap_content_button.is_active() != button.is_active() {
                    scrap_content_button.set_active(button.is_active());
                }
            })));
    }

    fn setup_more_actions_button(&self) {
        let imp = self.imp();

        let model = Menu::new();
        model.append(Some(&i18n("Export Article")), Some("win.export-article"));
        model.append(
            Some(&i18n("Open in browser")),
            Some("win.open-selected-article-in-browser"),
        );
        model.append(Some(&i18n("Toggle Fullscreen")), Some("win.fullscreen-article"));
        model.append(Some(&i18n("Close Article")), Some("win.close-article"));
        imp.more_actions_button.set_menu_model(Some(&model));
    }

    pub fn article_view(&self) -> &ArticleView {
        let imp = self.imp();
        &imp.article_view
    }

    pub fn back_button(&self) -> &Button {
        let imp = self.imp();
        &imp.back_button
    }

    pub fn headerbar(&self) -> &HeaderBar {
        let imp = self.imp();
        &imp.headerbar
    }

    pub fn header_squeezer(&self) -> &Squeezer {
        let imp = self.imp();
        &imp.header_squeezer
    }

    pub fn footer_revealer(&self) -> &Revealer {
        let imp = self.imp();
        &imp.footer_revealer
    }

    fn unread_button_state(article: Option<&FatArticle>) -> (&str, bool) {
        match article {
            Some(article) => match article.unread {
                Read::Read => ("read", false),
                Read::Unread => ("unread", true),
            },
            None => ("read", false),
        }
    }

    fn marked_button_state(article: Option<&FatArticle>) -> (&str, bool) {
        match article {
            Some(article) => match article.marked {
                Marked::Marked => ("marked", true),
                Marked::Unmarked => ("unmarked", false),
            },
            None => ("unmarked", false),
        }
    }

    pub fn show_article(&self, article: Option<&FatArticle>, enclosures: Option<&Vec<Enclosure>>) {
        let imp = self.imp();
        let content_page_state = App::default().content_page_state();
        let sensitive = article.is_some();
        let offline = content_page_state.read().get_offline();

        let (unread_icon, unread_active) = Self::unread_button_state(article);
        let (marked_icon, marked_active) = Self::marked_button_state(article);

        imp.mark_article_stack.set_visible_child_name(marked_icon);
        imp.mark_article_read_stack.set_visible_child_name(unread_icon);

        // block signals
        if let Some(signal_id) = imp.mark_article_read_event.read().as_ref() {
            imp.mark_article_read_button.block_signal(signal_id);
        }
        if let Some(signal_id) = imp.mark_article_event.read().as_ref() {
            imp.mark_article_button.block_signal(signal_id);
        }

        // set state
        imp.mark_article_button.set_active(marked_active);
        imp.mark_article_read_button.set_active(unread_active);

        // unblock signals
        if let Some(signal_id) = imp.mark_article_read_event.read().as_ref() {
            imp.mark_article_read_button.unblock_signal(signal_id);
        }
        if let Some(signal_id) = imp.mark_article_event.read().as_ref() {
            imp.mark_article_button.unblock_signal(signal_id);
        }

        let scrap_ongoing = content_page_state.read().is_article_scrap_ongoing();

        if scrap_ongoing {
            self.start_scrap_content_spinner();
        } else {
            let show_scraped_content = if let Some(fat_article) = article {
                fat_article.scraped_content.is_some() && content_page_state.read().get_prefer_scraped_content()
            } else {
                false
            };
            self.stop_scrap_content_spinner();
            self.update_scrape_content_button_state(show_scraped_content);
        }

        if let Some(enclosures) = enclosures {
            imp.enclosure_popover.update(enclosures);
            imp.footer_enclosure_popover.update(enclosures);

            imp.enclosure_button.set_popover(Some(&imp.enclosure_popover));
            imp.footer_enclosure_button
                .set_popover(Some(&imp.footer_enclosure_popover));
            imp.enclosure_popover.set_position(PositionType::Bottom);
            imp.footer_enclosure_popover.set_position(PositionType::Top);

            imp.enclosure_button.set_visible(true);
            imp.footer_enclosure_button.set_visible(true);
        } else {
            imp.enclosure_button.set_visible(false);
            imp.footer_enclosure_button.set_visible(false);
        }

        GtkUtil::action_set_enabled("close-article", sensitive);
        GtkUtil::action_set_enabled("export-article", sensitive);
        GtkUtil::action_set_enabled("open-selected-article-in-browser", sensitive);

        if !offline || !sensitive {
            let tag_support = App::default()
                .features()
                .read()
                .contains(PluginCapabilities::SUPPORT_TAGS);

            imp.mark_article_button.set_sensitive(sensitive);
            imp.mark_article_read_button.set_sensitive(sensitive);
            imp.scrap_content_button.set_sensitive(sensitive && !scrap_ongoing);
            imp.footer_scrap_content_button
                .set_sensitive(sensitive && !scrap_ongoing);
            self.update_tags(&article);
            imp.tag_button.set_sensitive(sensitive && tag_support);
            imp.footer_tag_button.set_sensitive(sensitive && tag_support);
            imp.share_button.set_sensitive(sensitive);
            imp.footer_share_button.set_sensitive(sensitive);
        }
    }

    pub fn update_scrape_content_button_state(&self, active: bool) {
        let imp = self.imp();

        // block signals
        if let Some(signal_id) = imp.scrap_content_event.read().as_ref() {
            imp.scrap_content_button.block_signal(signal_id);
        }
        if let Some(signal_id) = imp.footer_scrap_content_event.read().as_ref() {
            imp.footer_scrap_content_button.block_signal(signal_id);
        }

        // set state
        imp.scrap_content_button.set_active(active);
        imp.footer_scrap_content_button.set_active(active);

        // unblock signals
        if let Some(signal_id) = imp.scrap_content_event.read().as_ref() {
            imp.scrap_content_button.unblock_signal(signal_id);
        }
        if let Some(signal_id) = imp.footer_scrap_content_event.read().as_ref() {
            imp.footer_scrap_content_button.unblock_signal(signal_id);
        }
    }

    fn update_tags(&self, article: &Option<&FatArticle>) {
        let imp = self.imp();

        if let Some(article) = article {
            imp.tag_popover.set_article_id(&article.article_id);
            imp.footer_tag_popover.set_article_id(&article.article_id);

            // update popovers when one of them is popped up or down
            // to keep footer & header popover in sync
            imp.tag_button_click.connect_pressed(clone!(
                @weak self as column,
                @strong article.article_id as article_id => @default-panic, move |_gesture_click, times, _x, _y| {
                if times != 1 {
                    return
                }

                let imp = column.imp();
                imp.tag_popover.set_article_id(&article_id);
            }));
            imp.footer_tag_button_click.connect_pressed(clone!(
                @weak self as column,
                @strong article.article_id as article_id => @default-panic, move |_gesture_click, times, _x, _y| {
                    if times != 1 {
                        return
                    }

                    let imp = column.imp();
                    imp.footer_tag_popover.set_article_id(&article_id);
            }));
        }
    }

    pub fn popup_tag_popover(&self) {
        let imp = self.imp();
        if imp.footer_revealer.is_child_revealed() {
            imp.footer_tag_button.popup();
        } else {
            imp.tag_button.popup();
        }
    }

    pub fn start_scrap_content_spinner(&self) {
        let imp = self.imp();
        imp.scrap_content_button.set_sensitive(false);
        imp.footer_scrap_content_button.set_sensitive(false);
        imp.scrap_content_stack.set_visible_child_name("spinner");
        imp.footer_scrap_content_stack.set_visible_child_name("spinner");
    }

    pub fn stop_scrap_content_spinner(&self) {
        let imp = self.imp();
        let offline = App::default().content_page_state().read().get_offline();
        imp.scrap_content_button.set_sensitive(!offline);
        imp.footer_scrap_content_button.set_sensitive(!offline);

        // are we shoing scraped content right now?
        let (visible_article, _) = imp.article_view.get_visible_article();
        let scraped_content_visible = visible_article.map(|a| a.scraped_content.is_some()).unwrap_or(false);

        self.update_scrape_content_button_state(scraped_content_visible);

        imp.scrap_content_stack.set_visible_child_name("button");
        imp.footer_scrap_content_stack.set_visible_child_name("button");
    }

    pub fn start_more_actions_spinner(&self) {
        let imp = self.imp();
        imp.more_actions_button.set_sensitive(false);
        imp.more_actions_stack.set_visible_child_name("spinner");
    }

    pub fn stop_more_actions_spinner(&self) {
        let imp = self.imp();
        imp.more_actions_button.set_sensitive(true);
        imp.more_actions_stack.set_visible_child_name("button");
    }

    pub fn set_offline(&self, offline: bool) {
        let imp = self.imp();
        let tag_support = App::default()
            .features()
            .read()
            .contains(PluginCapabilities::SUPPORT_TAGS);

        imp.mark_article_button.set_sensitive(!offline);
        imp.mark_article_read_button.set_sensitive(!offline);
        imp.scrap_content_button.set_sensitive(!offline);
        imp.footer_scrap_content_button.set_sensitive(!offline);
        imp.tag_button.set_sensitive(!offline && tag_support);
        imp.footer_tag_button.set_sensitive(!offline && tag_support);
        imp.share_button.set_sensitive(!offline);
        imp.footer_share_button.set_sensitive(!offline);
    }

    pub fn update_share_popover(&self) {
        let imp = self.imp();

        let show_button = App::default().settings().read().get_any_share_services_enabled();
        imp.share_button.set_visible(show_button);
        imp.footer_share_button.set_visible(show_button);

        imp.share_popover.update();
        imp.footer_share_popover.update();
    }
}
