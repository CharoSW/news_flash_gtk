mod advanced;
mod article_list;
mod article_view;
mod dialog;
mod error;
mod feed_list;
mod general;
mod keybindings;
mod share;
mod user_data_size;

pub use self::advanced::{AdvancedSettings, ProxyModel, ProxyProtocoll};
use self::error::SettingsError;
use self::general::SyncInterval;
use self::share::ShareSettings;
pub use self::user_data_size::UserDataSize;
use crate::article_view::ArticleTheme;
use article_list::ArticleListSettings;
use article_view::ArticleViewSettings;
pub use dialog::SettingsDialog;
use feed_list::FeedListSettings;
use general::GeneralSettings;
pub use keybindings::Keybindings;
use news_flash::models::ArticleOrder;
use serde::{Deserialize, Serialize};
use std::fs;
use std::path::PathBuf;

static CONFIG_NAME: &str = "newsflash_gtk.json";

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Settings {
    general: GeneralSettings,
    advanced: AdvancedSettings,
    #[serde(default)]
    feed_list: FeedListSettings,
    article_list: ArticleListSettings,
    article_view: ArticleViewSettings,
    keybindings: Keybindings,
    #[serde(default)]
    share: ShareSettings,
    #[serde(skip_serializing)]
    #[serde(skip_deserializing)]
    path: PathBuf,
}

impl Settings {
    pub fn open() -> Result<Self, SettingsError> {
        let path = crate::app::CONFIG_DIR.join(CONFIG_NAME);
        if path.as_path().exists() {
            let data = fs::read_to_string(&path)?;
            if let Ok(mut settings) = serde_json::from_str::<Self>(&data) {
                settings.path = path;
                return Ok(settings);
            }
        }

        fs::create_dir_all(crate::app::CONFIG_DIR.as_path())?;

        let settings = Settings {
            general: GeneralSettings::default(),
            advanced: AdvancedSettings::default(),
            feed_list: FeedListSettings::default(),
            article_list: ArticleListSettings::default(),
            article_view: ArticleViewSettings::default(),
            keybindings: Keybindings::default(),
            share: ShareSettings::default(),
            path,
        };
        settings.write()?;
        Ok(settings)
    }

    fn write(&self) -> Result<(), SettingsError> {
        let data = serde_json::to_string_pretty(self)?;
        fs::write(&self.path, data)?;
        Ok(())
    }

    pub fn get_keep_running_in_background(&self) -> bool {
        self.general.keep_running_in_background
    }

    pub fn set_keep_running_in_background(&mut self, keep_running: bool) -> Result<(), SettingsError> {
        self.general.keep_running_in_background = keep_running;
        self.write()?;
        Ok(())
    }

    pub fn get_sync_on_startup(&self) -> bool {
        self.general.sync_on_startup
    }

    pub fn set_sync_on_startup(&mut self, sync_on_startup: bool) -> Result<(), SettingsError> {
        self.general.sync_on_startup = sync_on_startup;
        self.write()?;
        Ok(())
    }

    pub fn get_sync_on_metered(&self) -> bool {
        self.general.sync_on_metered
    }

    pub fn set_sync_on_metered(&mut self, sync_on_metered: bool) -> Result<(), SettingsError> {
        self.general.sync_on_metered = sync_on_metered;
        self.write()?;
        Ok(())
    }

    pub fn get_sync_interval(&self) -> SyncInterval {
        self.general.sync_every
    }

    pub fn set_sync_interval(&mut self, sync_every: SyncInterval) -> Result<(), SettingsError> {
        self.general.sync_every = sync_every;
        self.write()?;
        Ok(())
    }

    pub fn get_feed_list_only_show_relevant(&self) -> bool {
        self.feed_list.only_show_relevant
    }

    pub fn set_feed_list_only_show_relevant(&mut self, only_show_relevant: bool) -> Result<(), SettingsError> {
        self.feed_list.only_show_relevant = only_show_relevant;
        self.write()?;
        Ok(())
    }

    pub fn get_article_list_order(&self) -> ArticleOrder {
        self.article_list.order.clone()
    }

    pub fn set_article_list_order(&mut self, order: ArticleOrder) -> Result<(), SettingsError> {
        self.article_list.order = order;
        self.write()?;
        Ok(())
    }

    pub fn get_article_list_show_thumbs(&self) -> bool {
        self.article_list.show_thumbnails
    }

    pub fn set_article_list_show_thumbs(&mut self, show_thumbs: bool) -> Result<(), SettingsError> {
        self.article_list.show_thumbnails = show_thumbs;
        self.write()?;
        Ok(())
    }

    pub fn get_article_list_hide_future_articles(&self) -> bool {
        self.article_list.hide_future_articles
    }

    pub fn set_article_list_hide_future_articles(&mut self, hide_future_articles: bool) -> Result<(), SettingsError> {
        self.article_list.hide_future_articles = hide_future_articles;
        self.write()?;
        Ok(())
    }

    pub fn get_article_view_theme(&self) -> ArticleTheme {
        self.article_view.theme.clone()
    }

    pub fn set_article_view_theme(&mut self, theme: &ArticleTheme) -> Result<(), SettingsError> {
        self.article_view.theme = theme.clone();
        self.write()?;
        Ok(())
    }

    pub fn get_article_view_allow_select(&self) -> bool {
        self.article_view.allow_select
    }

    pub fn set_article_view_allow_select(&mut self, allow: bool) -> Result<(), SettingsError> {
        self.article_view.allow_select = allow;
        self.write()?;
        Ok(())
    }

    pub fn get_article_view_font(&self) -> Option<&str> {
        self.article_view.font.as_deref()
    }

    pub fn set_article_view_font(&mut self, font: Option<String>) -> Result<(), SettingsError> {
        self.article_view.font = font;
        self.write()?;
        Ok(())
    }

    pub fn get_article_view_width(&self) -> Option<u32> {
        self.article_view.content_width
    }

    pub fn set_article_view_width(&mut self, width: Option<u32>) -> Result<(), SettingsError> {
        self.article_view.content_width = width;
        self.write()?;
        Ok(())
    }

    pub fn get_article_view_line_height(&self) -> Option<f32> {
        self.article_view.line_height
    }

    pub fn set_article_view_line_height(&mut self, height: Option<f32>) -> Result<(), SettingsError> {
        self.article_view.line_height = height;
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_shortcut(&self) -> Option<&str> {
        self.keybindings.general.shortcut.as_deref()
    }

    pub fn set_keybind_shortcut(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.general.shortcut = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_refresh(&self) -> Option<&str> {
        self.keybindings.general.refresh.as_deref()
    }

    pub fn set_keybind_refresh(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.general.refresh = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_search(&self) -> Option<&str> {
        self.keybindings.general.search.as_deref()
    }

    pub fn set_keybind_search(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.general.search = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_quit(&self) -> Option<&str> {
        self.keybindings.general.quit.as_deref()
    }

    pub fn set_keybind_quit(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.general.quit = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_all_articles(&self) -> Option<&str> {
        self.keybindings.general.all_articles.as_deref()
    }

    pub fn set_keybind_all_articles(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.general.all_articles = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_only_unread(&self) -> Option<&str> {
        self.keybindings.general.only_unread.as_deref()
    }

    pub fn set_keybind_only_unread(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.general.only_unread = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_only_starred(&self) -> Option<&str> {
        self.keybindings.general.only_starred.as_deref()
    }

    pub fn set_keybind_only_starred(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.general.only_starred = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_view_up(&self) -> Option<&str> {
        self.keybindings.article_view.scroll_up.as_deref()
    }

    pub fn set_keybind_article_view_up(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.article_view.scroll_up = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_view_down(&self) -> Option<&str> {
        self.keybindings.article_view.scroll_down.as_deref()
    }

    pub fn set_keybind_article_view_down(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.article_view.scroll_down = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_view_scrap(&self) -> Option<&str> {
        self.keybindings.article_view.scrap_content.as_deref()
    }

    pub fn set_keybind_article_view_scrap(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.article_view.scrap_content = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_view_tag(&self) -> Option<&str> {
        self.keybindings.article_view.tag.as_deref()
    }

    pub fn set_keybind_article_view_tag(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.article_view.tag = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_view_fullscreen(&self) -> Option<&str> {
        self.keybindings.article_view.fullscreen.as_deref()
    }

    pub fn set_keybind_article_view_fullscreen(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.article_view.fullscreen = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_fullscreen(&self) -> Option<&str> {
        self.keybindings.article_view.fullscreen.as_deref()
    }

    pub fn set_keybind_article_fullscreen(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.article_view.fullscreen = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_list_next(&self) -> Option<&str> {
        self.keybindings.article_list.next.as_deref()
    }

    pub fn set_keybind_article_list_next(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.article_list.next = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_list_prev(&self) -> Option<&str> {
        self.keybindings.article_list.prev.as_deref()
    }

    pub fn set_keybind_article_list_prev(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.article_list.prev = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_list_read(&self) -> Option<&str> {
        self.keybindings.article_list.read.as_deref()
    }

    pub fn set_keybind_article_list_read(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.article_list.read = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_list_mark(&self) -> Option<&str> {
        self.keybindings.article_list.mark.as_deref()
    }

    pub fn set_keybind_article_list_mark(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.article_list.mark = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_list_open(&self) -> Option<&str> {
        self.keybindings.article_list.open.as_deref()
    }

    pub fn set_keybind_article_list_open(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.article_list.open = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_article_list_copy_url(&self) -> Option<&str> {
        self.keybindings.article_list.copy_url.as_deref()
    }

    pub fn set_keybind_article_list_copy_url(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.article_list.copy_url = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_feed_list_next(&self) -> Option<&str> {
        self.keybindings.feed_list.next.as_deref()
    }

    pub fn set_keybind_feed_list_next(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.feed_list.next = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_feed_list_prev(&self) -> Option<&str> {
        self.keybindings.feed_list.prev.as_deref()
    }

    pub fn set_keybind_feed_list_prev(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.feed_list.prev = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_feed_list_toggle_expanded(&self) -> Option<&str> {
        self.keybindings.feed_list.toggle_expanded.as_deref()
    }

    pub fn set_keybind_feed_list_toggle_expanded(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.feed_list.toggle_expanded = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_keybind_sidebar_set_read(&self) -> Option<&str> {
        self.keybindings.feed_list.read.as_deref()
    }

    pub fn set_keybind_sidebar_set_read(&mut self, key: Option<&str>) -> Result<(), SettingsError> {
        self.keybindings.feed_list.read = key.map(str::to_string);
        self.write()?;
        Ok(())
    }

    pub fn get_accept_invalid_certs(&self) -> bool {
        self.advanced.accept_invalid_certs
    }

    pub fn set_accept_invalid_certs(&mut self, accept_invalid_certs: bool) -> Result<(), SettingsError> {
        self.advanced.accept_invalid_certs = accept_invalid_certs;
        self.write()?;
        Ok(())
    }

    pub fn get_inspect_article_view(&self) -> bool {
        self.advanced.inspect_article_view
    }

    pub fn set_inspect_article_view(&mut self, inspect_article_view: bool) {
        self.advanced.inspect_article_view = inspect_article_view;
    }

    pub fn get_article_view_load_images(&self) -> bool {
        self.advanced.article_view_load_images
    }

    pub fn set_article_view_load_images(&mut self, article_view_load_images: bool) {
        self.advanced.article_view_load_images = article_view_load_images;
    }

    pub fn get_accept_invalid_hostnames(&self) -> bool {
        self.advanced.accept_invalid_hostnames
    }

    pub fn set_accept_invalid_hostnames(&mut self, accept_invalid_hostnames: bool) -> Result<(), SettingsError> {
        self.advanced.accept_invalid_hostnames = accept_invalid_hostnames;
        self.write()?;
        Ok(())
    }

    pub fn get_proxy(&self) -> Vec<ProxyModel> {
        self.advanced.proxy.clone()
    }

    pub fn get_any_share_services_enabled(&self) -> bool {
        self.share.pocket_enabled
            || self.share.instapaper_enabled
            || self.share.twitter_enabled
            || self.share.mastodon_enabled
            || self.share.reddit_enabled
            || self.share.telegram_enabled
            || self.share.custom_enabled
    }

    pub fn get_share_pocket_enabled(&self) -> bool {
        self.share.pocket_enabled
    }

    pub fn set_share_pocket_enabled(&mut self, pocket_enabled: bool) -> Result<(), SettingsError> {
        self.share.pocket_enabled = pocket_enabled;
        self.write()?;
        Ok(())
    }

    pub fn get_share_instapaper_enabled(&self) -> bool {
        self.share.instapaper_enabled
    }

    pub fn set_share_instapaper_enabled(&mut self, instapaper_enabled: bool) -> Result<(), SettingsError> {
        self.share.instapaper_enabled = instapaper_enabled;
        self.write()?;
        Ok(())
    }

    pub fn get_share_twitter_enabled(&self) -> bool {
        self.share.twitter_enabled
    }

    pub fn set_share_twitter_enabled(&mut self, twitter_enabled: bool) -> Result<(), SettingsError> {
        self.share.twitter_enabled = twitter_enabled;
        self.write()?;
        Ok(())
    }

    pub fn get_share_mastodon_enabled(&self) -> bool {
        self.share.mastodon_enabled
    }

    pub fn set_share_mastodon_enabled(&mut self, mastodon_enabled: bool) -> Result<(), SettingsError> {
        self.share.mastodon_enabled = mastodon_enabled;
        self.write()?;
        Ok(())
    }

    pub fn get_share_reddit_enabled(&self) -> bool {
        self.share.reddit_enabled
    }

    pub fn set_share_reddit_enabled(&mut self, reddit_enabled: bool) -> Result<(), SettingsError> {
        self.share.reddit_enabled = reddit_enabled;
        self.write()?;
        Ok(())
    }

    pub fn get_share_telegram_enabled(&self) -> bool {
        self.share.telegram_enabled
    }

    pub fn set_share_telegram_enabled(&mut self, telegram_enabled: bool) -> Result<(), SettingsError> {
        self.share.telegram_enabled = telegram_enabled;
        self.write()?;
        Ok(())
    }

    pub fn get_share_custom_enabled(&self) -> bool {
        self.share.custom_enabled
    }

    pub fn set_share_custom_enabled(&mut self, custom_enabled: bool) -> Result<(), SettingsError> {
        self.share.custom_enabled = custom_enabled;
        self.write()?;
        Ok(())
    }

    pub fn get_share_custom_name(&self) -> Option<&str> {
        self.share.custom_name.as_deref()
    }

    pub fn set_share_custom_name(&mut self, custom_name: Option<String>) -> Result<(), SettingsError> {
        self.share.custom_name = custom_name;
        self.write()?;
        Ok(())
    }

    pub fn get_share_custom_url(&self) -> Option<&str> {
        self.share.custom_url.as_deref()
    }

    pub fn set_share_custom_url(&mut self, custom_url: Option<String>) -> Result<(), SettingsError> {
        self.share.custom_url = custom_url;
        self.write()?;
        Ok(())
    }
}
