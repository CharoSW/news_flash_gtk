use super::error::SettingsError;
use crate::app::App;
use crate::settings::Settings;
use gdk4::{Key, ModifierType};
use log::warn;
use parking_lot::RwLock;
use serde::{Deserialize, Serialize};
use std::default::Default;
use std::str;
use std::sync::Arc;

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Keybindings {
    pub general: KeybindingsGeneral,
    pub article_view: KeybindingsArticleView,
    pub article_list: KeybindingsArticleList,
    pub feed_list: KeybindingsFeedList,
}

impl Keybindings {
    pub fn parse_shortcut_string(keybinding: &str) -> Option<String> {
        if let Some((keyval, modifier)) = gtk4::accelerator_parse(&keybinding) {
            Self::parse_shortcut(keyval, modifier)
        } else {
            None
        }
    }

    pub fn parse_shortcut(keyval: Key, modifier: ModifierType) -> Option<String> {
        let keyval = Self::parse_keyval(keyval);
        let modifier = Self::parse_modifiers(modifier);
        match keyval {
            None => None,
            Some(keyval) => match modifier {
                Some(mut modifier) => {
                    modifier.push_str(&keyval);
                    Some(modifier)
                }
                None => Some(keyval),
            },
        }
    }

    pub fn parse_keyval(keyval: Key) -> Option<String> {
        let manual_parsed = match keyval.to_lower() {
            Key::Shift_L
            | Key::Control_L
            | Key::Alt_L
            | Key::Meta_L
            | Key::Super_L
            | Key::Hyper_L
            | Key::Shift_R
            | Key::Control_R
            | Key::Alt_R
            | Key::Meta_R
            | Key::Super_R
            | Key::Hyper_R => Self::get_modifier_label(&keyval),
            Key::Left => Some("←".to_owned()),
            Key::Right => Some("→".to_owned()),
            Key::Down => Some("↓".to_owned()),
            Key::Up => Some("↑".to_owned()),
            Key::space => Some("␣".to_owned()),
            Key::Return => Some("⏎".to_owned()),
            Key::Delete => Some("Del".to_owned()),
            Key::Page_Up => Some("⇑".to_owned()),
            Key::Page_Down => Some("⇓".to_owned()),
            Key::BackSpace => Some("Backspace".to_owned()),
            Key::F1 => Some("F1".to_owned()),
            Key::F2 => Some("F2".to_owned()),
            Key::F3 => Some("F3".to_owned()),
            Key::F4 => Some("F4".to_owned()),
            Key::F5 => Some("F5".to_owned()),
            Key::F6 => Some("F6".to_owned()),
            Key::F7 => Some("F7".to_owned()),
            Key::F8 => Some("F8".to_owned()),
            Key::F9 => Some("F9".to_owned()),
            Key::F10 => Some("F10".to_owned()),
            Key::F11 => Some("F11".to_owned()),
            Key::F12 => Some("F12".to_owned()),
            _ => None,
        };

        if manual_parsed.is_some() {
            return manual_parsed;
        }

        match keyval.to_unicode() {
            Some(keyval) => {
                let mut buffer: [u8; 4] = [0; 4];
                Some(keyval.encode_utf8(&mut buffer).to_owned())
            }
            None => None,
        }
    }

    fn get_modifier_label(keyval: &Key) -> Option<String> {
        let mut mod_string = String::new();
        match keyval {
            &Key::Shift_L | &Key::Control_L | &Key::Alt_L | &Key::Meta_L | &Key::Super_L | &Key::Hyper_L => {
                mod_string.push('L')
            }
            &Key::Shift_R | &Key::Control_R | &Key::Alt_R | &Key::Meta_R | &Key::Super_R | &Key::Hyper_R => {
                mod_string.push('R')
            }
            _ => return None,
        }

        match keyval {
            &Key::Shift_L | &Key::Shift_R => mod_string.push_str("Shift"),
            &Key::Control_L | &Key::Control_R => mod_string.push_str("Ctrl"),
            &Key::Meta_L | &Key::Meta_R => mod_string.push_str("Meta"),
            &Key::Super_L | &Key::Super_R => mod_string.push_str("Super"),
            &Key::Hyper_L | &Key::Hyper_R => mod_string.push_str("Hyper"),
            _ => return None,
        }

        Some(mod_string)
    }

    fn parse_modifiers(modifier: ModifierType) -> Option<String> {
        let mut mod_string = String::new();

        if modifier.contains(ModifierType::SHIFT_MASK) {
            mod_string.push_str("Shift+");
        }
        if modifier.contains(ModifierType::CONTROL_MASK) {
            mod_string.push_str("Ctrl+");
        }
        if modifier.contains(ModifierType::ALT_MASK) {
            mod_string.push_str("Alt+");
        }
        if modifier.contains(ModifierType::SUPER_MASK) {
            mod_string.push_str("Super+");
        }
        if modifier.contains(ModifierType::HYPER_MASK) {
            mod_string.push_str("Hyper+");
        }
        if modifier.contains(ModifierType::META_MASK) {
            mod_string.push_str("Meta+");
        }

        if mod_string.is_empty() {
            return None;
        }

        Some(mod_string)
    }

    pub fn clean_modifier(mut modifier: ModifierType) -> ModifierType {
        modifier.remove(ModifierType::LOCK_MASK);
        modifier.remove(ModifierType::BUTTON1_MASK);
        modifier.remove(ModifierType::BUTTON2_MASK);
        modifier.remove(ModifierType::BUTTON3_MASK);
        modifier.remove(ModifierType::BUTTON4_MASK);
        modifier.remove(ModifierType::BUTTON5_MASK);

        modifier
    }

    pub fn write_keybinding(
        id: &str,
        keybinding: Option<&str>,
        settings: &Arc<RwLock<Settings>>,
    ) -> Result<(), SettingsError> {
        match id {
            "next_article" => settings.write().set_keybind_article_list_next(keybinding),
            "previous_article" => settings.write().set_keybind_article_list_prev(keybinding),
            "toggle_read" => settings.write().set_keybind_article_list_read(keybinding),
            "toggle_marked" => settings.write().set_keybind_article_list_mark(keybinding),
            "open_browser" => settings.write().set_keybind_article_list_open(keybinding),
            "copy_url" => settings.write().set_keybind_article_list_copy_url(keybinding),
            "next_item" => settings.write().set_keybind_feed_list_next(keybinding),
            "previous_item" => settings.write().set_keybind_feed_list_prev(keybinding),
            "toggle_category_expanded" => settings.write().set_keybind_feed_list_toggle_expanded(keybinding),
            "sidebar_set_read" => settings.write().set_keybind_sidebar_set_read(keybinding),
            "shortcuts" => settings.write().set_keybind_shortcut(keybinding),
            "refresh" => settings.write().set_keybind_refresh(keybinding),
            "search" => settings.write().set_keybind_search(keybinding),
            "quit" => settings.write().set_keybind_quit(keybinding),
            "all_articles" => settings.write().set_keybind_all_articles(keybinding),
            "only_unread" => settings.write().set_keybind_only_unread(keybinding),
            "only_starred" => settings.write().set_keybind_only_starred(keybinding),
            "scroll_up" => settings.write().set_keybind_article_view_up(keybinding),
            "scroll_down" => settings.write().set_keybind_article_view_down(keybinding),
            "scrap_content" => settings.write().set_keybind_article_view_scrap(keybinding),
            "tag" => settings.write().set_keybind_article_view_tag(keybinding),
            "fullscreen" => settings.write().set_keybind_article_fullscreen(keybinding),
            _ => {
                warn!("unexpected keybind id: {}", id);
                Err(SettingsError::InvalidKeybind(id.into()))
            }
        }?;
        App::default().main_window().setup_shortcuts();
        Ok(())
    }

    pub fn read_keybinding(id: &str) -> Result<Option<String>, SettingsError> {
        let settings = App::default().settings();
        match id {
            "next_article" => Ok(settings.read().get_keybind_article_list_next().map(str::to_string)),
            "previous_article" => Ok(settings.read().get_keybind_article_list_prev().map(str::to_string)),
            "toggle_read" => Ok(settings.read().get_keybind_article_list_read().map(str::to_string)),
            "toggle_marked" => Ok(settings.read().get_keybind_article_list_mark().map(str::to_string)),
            "open_browser" => Ok(settings.read().get_keybind_article_list_open().map(str::to_string)),
            "copy_url" => Ok(settings.write().get_keybind_article_list_copy_url().map(str::to_string)),
            "next_item" => Ok(settings.read().get_keybind_feed_list_next().map(str::to_string)),
            "previous_item" => Ok(settings.read().get_keybind_feed_list_prev().map(str::to_string)),
            "toggle_category_expanded" => Ok(settings
                .read()
                .get_keybind_feed_list_toggle_expanded()
                .map(str::to_string)),
            "sidebar_set_read" => Ok(settings.read().get_keybind_sidebar_set_read().map(str::to_string)),
            "shortcuts" => Ok(settings.read().get_keybind_shortcut().map(str::to_string)),
            "refresh" => Ok(settings.read().get_keybind_refresh().map(str::to_string)),
            "search" => Ok(settings.read().get_keybind_search().map(str::to_string)),
            "quit" => Ok(settings.read().get_keybind_quit().map(str::to_string)),
            "all_articles" => Ok(settings.read().get_keybind_all_articles().map(str::to_string)),
            "only_unread" => Ok(settings.read().get_keybind_only_unread().map(str::to_string)),
            "only_starred" => Ok(settings.read().get_keybind_only_starred().map(str::to_string)),
            "scroll_up" => Ok(settings.read().get_keybind_article_view_up().map(str::to_string)),
            "scroll_down" => Ok(settings.read().get_keybind_article_view_down().map(str::to_string)),
            "scrap_content" => Ok(settings.read().get_keybind_article_view_scrap().map(str::to_string)),
            "tag" => Ok(settings.read().get_keybind_article_view_tag().map(str::to_string)),
            "fullscreen" => Ok(settings.read().get_keybind_article_fullscreen().map(str::to_string)),
            _ => {
                warn!("unexpected keybind id: {}", id);
                Err(SettingsError::InvalidKeybind(id.into()))
            }
        }
    }
}

//--------------------------------------------
// General
//--------------------------------------------

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct KeybindingsGeneral {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub shortcut: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub refresh: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub search: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub quit: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub all_articles: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub only_unread: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub only_starred: Option<String>,
}

impl Default for KeybindingsGeneral {
    fn default() -> Self {
        KeybindingsGeneral {
            shortcut: Some("F1".to_owned()),
            refresh: Some("F5".to_owned()),
            search: Some("<ctl>F".to_owned()),
            quit: Some("<ctl>Q".to_owned()),
            all_articles: Some("<ctl>1".to_owned()),
            only_unread: Some("<ctl>2".to_owned()),
            only_starred: Some("<ctl>3".to_owned()),
        }
    }
}

//--------------------------------------------
// Article View
//--------------------------------------------

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct KeybindingsArticleView {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub scroll_up: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub scroll_down: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub scrap_content: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub tag: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub fullscreen: Option<String>,
}

impl Default for KeybindingsArticleView {
    fn default() -> Self {
        KeybindingsArticleView {
            scroll_up: Some("I".into()),
            scroll_down: Some("U".into()),
            scrap_content: Some("<Shift>C".into()),
            tag: Some("<ctl>T".into()),
            fullscreen: Some("<Shift><alt>X".into()),
        }
    }
}

//--------------------------------------------
// Article List
//--------------------------------------------

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct KeybindingsArticleList {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub next: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub prev: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub read: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub mark: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub open: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub copy_url: Option<String>,
}

impl Default for KeybindingsArticleList {
    fn default() -> Self {
        KeybindingsArticleList {
            next: Some("J".to_owned()),
            prev: Some("K".to_owned()),
            read: Some("R".to_owned()),
            mark: Some("M".to_owned()),
            open: Some("O".to_owned()),
            copy_url: Some("<ctl><Shift>C".to_owned()),
        }
    }
}

//--------------------------------------------
// Feed List
//--------------------------------------------

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct KeybindingsFeedList {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub next: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub prev: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub toggle_expanded: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub read: Option<String>,
}

impl Default for KeybindingsFeedList {
    fn default() -> Self {
        KeybindingsFeedList {
            next: Some("<ctl>J".to_owned()),
            prev: Some("<ctl>K".to_owned()),
            toggle_expanded: Some("C".to_owned()),
            read: Some("<Shift>A".to_owned()),
        }
    }
}
