use chrono::Duration;
use glib::Enum;
use serde::{Deserialize, Serialize};
use std::default::Default;
use std::fmt;

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq)]
pub enum SyncInterval {
    Never,
    Predefined(PredefinedSyncInterval),
    Custom(u32), // Seconds
}

impl SyncInterval {
    pub fn to_minutes(&self) -> Option<u32> {
        match self {
            Self::Never => None,
            Self::Predefined(pre) => pre.to_minutes(),
            Self::Custom(duration) => Some(duration / 60),
        }
    }

    pub fn to_seconds(&self) -> Option<u32> {
        match self {
            Self::Never => None,
            Self::Predefined(pre) => pre.to_seconds(),
            Self::Custom(duration) => Some(*duration),
        }
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq, Enum)]
#[repr(u32)]
#[enum_type(name = "SyncInterval")]
pub enum PredefinedSyncInterval {
    QuaterHour,
    HalfHour,
    Hour,
    TwoHour,
}

impl fmt::Display for PredefinedSyncInterval {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let text = match self {
            Self::QuaterHour => "15 Minutes",
            Self::HalfHour => "30 Minutes",
            Self::Hour => "1 Hour",
            Self::TwoHour => "2 Hours",
        };

        write!(f, "{}", text)
    }
}

impl PredefinedSyncInterval {
    pub fn to_minutes(&self) -> Option<u32> {
        match self {
            Self::QuaterHour => Some(15),
            Self::HalfHour => Some(30),
            Self::Hour => Some(60),
            Self::TwoHour => Some(120),
        }
    }

    pub fn to_seconds(&self) -> Option<u32> {
        self.to_minutes().map(|m| m * 60)
    }

    pub fn from_u32(v: u32) -> Self {
        match v {
            0 => Self::QuaterHour,
            1 => Self::HalfHour,
            2 => Self::Hour,
            3 => Self::TwoHour,
            _ => Self::QuaterHour,
        }
    }

    pub fn to_u32(&self) -> u32 {
        match self {
            Self::QuaterHour => 0,
            Self::HalfHour => 1,
            Self::Hour => 2,
            Self::TwoHour => 3,
        }
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq, Enum)]
#[repr(u32)]
#[enum_type(name = "KeepArticlesDuration")]
pub enum KeepArticlesDuration {
    Forever,
    OneYear,
    SixMonths,
    OneMonth,
    OneWeek,
}

impl fmt::Display for KeepArticlesDuration {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let text = match self {
            Self::Forever => "Forever",
            Self::OneYear => "One Year",
            Self::SixMonths => "6 Months",
            Self::OneMonth => "One Month",
            Self::OneWeek => "One Week",
        };

        write!(f, "{}", text)
    }
}

impl KeepArticlesDuration {
    pub fn to_duration(&self) -> Option<Duration> {
        match self {
            Self::Forever => None,
            Self::OneYear => Some(Duration::days(365)),
            Self::SixMonths => Some(Duration::days(182)),
            Self::OneMonth => Some(Duration::days(30)),
            Self::OneWeek => Some(Duration::days(7)),
        }
    }

    pub fn from_duration(duration: Option<Duration>) -> Self {
        if let Some(duration) = duration {
            match duration.num_days() {
                365 => Self::OneYear,
                182 => Self::SixMonths,
                30 => Self::OneMonth,
                7 => Self::OneWeek,

                _ => Self::Forever,
            }
        } else {
            KeepArticlesDuration::Forever
        }
    }

    pub fn to_u32(&self) -> u32 {
        match self {
            Self::Forever => 0,
            Self::OneYear => 1,
            Self::SixMonths => 2,
            Self::OneMonth => 3,
            Self::OneWeek => 4,
        }
    }

    pub fn from_u32(i: u32) -> Self {
        match i {
            0 => Self::Forever,
            1 => Self::OneYear,
            2 => Self::SixMonths,
            3 => Self::OneMonth,
            4 => Self::OneWeek,

            _ => Self::Forever,
        }
    }
}

const fn _default_true() -> bool {
    true
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct GeneralSettings {
    #[serde(default = "bool::default")]
    pub keep_running_in_background: bool,
    #[serde(default = "bool::default")]
    pub sync_on_startup: bool,
    #[serde(default = "_default_true")]
    pub sync_on_metered: bool,
    pub sync_every: SyncInterval,
}

impl Default for GeneralSettings {
    fn default() -> Self {
        GeneralSettings {
            keep_running_in_background: false,
            sync_on_startup: false,
            sync_on_metered: true,
            sync_every: SyncInterval::Predefined(PredefinedSyncInterval::QuaterHour),
        }
    }
}
