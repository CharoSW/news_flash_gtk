use crate::app::Action;
use crate::util::Util;
use glib::{clone, subclass::*};
use gtk4::{prelude::*, subclass::prelude::*, Box, Button, CompositeTemplate, Entry, Widget};

mod imp {
    use super::*;
    use glib::subclass;
    use once_cell::sync::Lazy;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/add_category_widget.ui")]
    pub struct AddCategoryWidget {
        #[template_child]
        pub add_category_button: TemplateChild<Button>,
        #[template_child]
        pub category_entry: TemplateChild<Entry>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AddCategoryWidget {
        const NAME: &'static str = "AddCategoryWidget";
        type ParentType = Box;
        type Type = super::AddCategoryWidget;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AddCategoryWidget {
        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| vec![Signal::builder("category-added").build()]);
            SIGNALS.as_ref()
        }

        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for AddCategoryWidget {}

    impl BoxImpl for AddCategoryWidget {}
}

glib::wrapper! {
    pub struct AddCategoryWidget(ObjectSubclass<imp::AddCategoryWidget>)
        @extends Widget, Box;
}

impl AddCategoryWidget {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[])
    }

    fn init(&self) {
        let imp = self.imp();

        let add_category_button = imp.add_category_button.get();
        let category_entry = imp.category_entry.get();

        // make parse button sensitive if entry contains text and vice versa
        imp.category_entry
            .connect_changed(clone!(@weak add_category_button => @default-panic, move |entry| {
                add_category_button.set_sensitive(!entry.text().as_str().is_empty());

                entry.set_secondary_icon_name(None);
                entry.set_secondary_icon_tooltip_text(None);
            }));

        // hit enter in entry to add category
        imp.category_entry
            .connect_activate(clone!(@weak add_category_button => @default-panic, move |_entry| {
                if add_category_button.get_sensitive() {
                    add_category_button.emit_clicked();
                }
            }));

        imp.add_category_button.connect_clicked(clone!(
            @weak self as widget,
            @weak category_entry => @default-panic, move |_button|
        {
            if !category_entry.text().as_str().is_empty() {
                Util::send(Action::AddCategory(category_entry.text().as_str().into()));
                widget.emit_by_name::<()>("category-added", &[]);
            }
        }));
    }

    pub fn reset(&self) {
        self.imp().category_entry.set_text("");
    }
}
