use crate::util::GtkUtil;
use glib::subclass;
use gtk4::{self, prelude::*, subclass::prelude::*, CompositeTemplate, Image, Widget};
use libadwaita::prelude::*;
use libadwaita::subclass::prelude::*;
use libadwaita::{ActionRow, PreferencesRow};
use news_flash::models::{PluginIcon, PluginInfo};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/service_row.ui")]
    pub struct ServiceRow {
        #[template_child]
        pub icon: TemplateChild<Image>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ServiceRow {
        const NAME: &'static str = "ServiceRow";
        type ParentType = ActionRow;
        type Type = super::ServiceRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ServiceRow {}

    impl WidgetImpl for ServiceRow {}

    impl ListBoxRowImpl for ServiceRow {}

    impl PreferencesRowImpl for ServiceRow {}

    impl ActionRowImpl for ServiceRow {}
}

glib::wrapper! {
    pub struct ServiceRow(ObjectSubclass<imp::ServiceRow>)
        @extends Widget, ActionRow, PreferencesRow;
}

impl ServiceRow {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[])
    }

    pub fn init(&self, info: &PluginInfo) {
        let imp = self.imp();

        // title
        self.set_title(&info.name);

        // icon
        imp.icon.set_from_icon_name(Some("feed-service-generic"));
        let scale = GtkUtil::get_scale(self);
        if let Some(icon) = &info.icon {
            let texture = match icon {
                PluginIcon::Vector(icon) => GtkUtil::create_texture_from_bytes(&icon.data, 64, 64, scale)
                    .expect("Failed to create surface from service vector icon."),
                PluginIcon::Pixel(icon) => GtkUtil::create_texture_from_pixelicon(icon, scale)
                    .expect("Failed to create surface from service pixel icon."),
            };
            imp.icon.set_from_paintable(Some(&texture));
        }

        // // Website
        // if let Some(website) = info.website {
        //     let website_string = website.to_string();
        //     imp.website_label
        //         .set_markup(&format!("<a href=\"{}\">{}</a>", website_string, website_string));
        // } else {
        //     imp.website_label.set_text(&i18n("No Website"));
        // }

        // let unknown = i18n("Unknown Free License");
        // let sad_panda = i18n("Proprietary Software");
        // let license_string = match info.license_type {
        //     ServiceLicense::ApacheV2 => "<a href=\"http://www.apache.org/licenses/LICENSE-2.0\">Apache v2</a>",
        //     ServiceLicense::AGPLv3 => "<a href=\"https://www.gnu.org/licenses/agpl-3.0.en.html\">AGPLv3</a>",
        //     ServiceLicense::GPLv2 => "<a href=\"http://www.gnu.de/documents/gpl-2.0.en.html\">GPLv2</a>",
        //     ServiceLicense::GPLv3 => "<a href=\"http://www.gnu.de/documents/gpl-3.0.en.html\">GPLv3</a>",
        //     ServiceLicense::MIT => "<a href=\"https://opensource.org/licenses/MIT\">MIT</a>",
        //     ServiceLicense::LGPLv21 => "<a href=\"http://www.gnu.de/documents/lgpl-2.1.en.html\">LGPLv2.1</a>",
        //     ServiceLicense::GenericFree => &unknown,
        //     ServiceLicense::GenericProprietary => &sad_panda,
        // };
        // imp.license_label.set_markup(license_string);

        // let type_string = match info.service_type {
        //     ServiceType::Local => i18n("Local data only"),
        //     ServiceType::Remote { self_hosted } => {
        //         if self_hosted {
        //             i18n("Synced and self-hosted")
        //         } else {
        //             i18n("Synced")
        //         }
        //     }
        // };
        // imp.type_label.set_text(&type_string);

        // let price_string = match info.service_price {
        //     ServicePrice::Free => i18n("Free"),
        //     ServicePrice::Paid => i18n("Paid"),
        //     ServicePrice::PaidPremimum => i18n("Free / Paid Premium"),
        // };
        // imp.price_label.set_text(&price_string);
    }
}
